#!/usr/bin/env python3

from arithmetic import arithmetic_decode
from sys import argv
from binary import coded_bytes_to_bits


def camunzip3(filename):
    if '.cz3' not in filename:
        raise ValueError('The filename you passed in is not a CamZip 3 file')

    file = open(filename, 'rb')
    ciphertext = file.read()
    file.close()
    file_bytes = []

    for data in ciphertext:
        file_bytes.append(data)

    file_bin = coded_bytes_to_bits(file_bytes)

    file = open(filename + 'c', 'r')
    file_length = int(file.readline().rstrip())
    probs = [float(line.rstrip()) for line in file.readlines()]
    file.close()

    # decode the document
    plaintext = arithmetic_decode(file_bin, probs, file_length)

    # write to the output file in binary
    file = open(filename.replace('.cz3', '.uz3'), 'wb')
    b = bytearray()
    b.extend(map(ord, plaintext))
    file.write(b)
    file.close()


# read the filename from the arguments to the function
f = argv[1]
camunzip3(f)

import numpy as np


def yuv_to_rgb(pixel):
    # yuv_to_rgb(pixel) converts the YUV colour values Y, U and V into
    # the RGB colour space, and returns an equivalent R, G and B
    # 3x1 array

    # first convert to numpy array
    pixel = np.array(pixel)

    if np.shape(pixel) != (3,):
        raise ValueError('The input value is not a pixel, which should be a 1D array of length 3')

    # remove YUV offsets from the pixel
    pixel -= np.array([0, 128, 128])

    # turn pixel into a column vector, we don't need to normalise by 255
    # because we are looking to output in 8-bit integer format
    pixel = pixel.reshape(pixel.size, 1)

    # setup the transformation matrix from rgb to yuv
    rgb_transformation_matrix = np.matrix([
        [1, 0, 1.402],
        [1, -0.34414, -0.71414],
        [1, 1.772, 0],
    ])

    # generate the RGB pixel by matrix multiplication
    rgb_pixel = rgb_transformation_matrix * pixel

    # turn rgb_pixel back into a row vector
    rgb_pixel = rgb_pixel.reshape(rgb_pixel.size,)

    # round each component
    rgb_pixel = rgb_pixel.round()

    # turn rgb pixel into a list
    rgb_pixel = rgb_pixel.tolist()[0]

    # return the RGB pixel
    return rgb_pixel

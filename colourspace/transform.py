import numpy as np


def transform(image, transformation):
    # transform(image, transformation) takes a 2D array of 3x1 arrays,
    # each representing 3 colour values (e.g. in the RGB or YUV colour
    # spaces), and applies the transformation to them
    #
    # intended to be used to transform an entire image from one colour
    # space to another and back again
    #
    # Copyright Joshua 2017

    # convert the image into a numpy array
    image = np.array(image)

    # get the shape of the array, so we can iterate over each pixel
    shape = np.shape(image)

    # create an output image
    output_image = np.zeros(shape)

    # iterate over each pixel and apply the transformation
    for y in range(0, shape[0]):
        for x in range(0, shape[1]):
            output_image[y, x] = transformation(image[y, x])

    # return the output image
    return output_image

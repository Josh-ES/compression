import unittest
import numpy as np
import random
from .rgb_to_yuv import rgb_to_yuv
from .transform import transform


class TestPixelTransformerMethod(unittest.TestCase):

    def testGeneratesCorrectYUVValues(self):
        # create a pink RGB pixel
        px = [236, 30, 98]

        # create an image of size 8x8
        image = [
            [px, px, px, px, px, px, px, px],
            [px, px, px, px, px, px, px, px],
            [px, px, px, px, px, px, px, px],
            [px, px, px, px, px, px, px, px],
            [px, px, px, px, px, px, px, px],
            [px, px, px, px, px, px, px, px],
            [px, px, px, px, px, px, px, px],
            [px, px, px, px, px, px, px, px],
        ]

        # apply the transformation
        transformed_image = transform(image, rgb_to_yuv)

        # pick a random pixel in the image
        x = random.randint(0, 7)
        y = random.randint(0, 7)

        # assert that the pixel is the correct colour
        np.testing.assert_array_equal(transformed_image[x, y], [99, 127, 225])


if __name__ == '__main__':
    unittest.main()

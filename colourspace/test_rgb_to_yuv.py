import unittest
import numpy as np
from .rgb_to_yuv import rgb_to_yuv


class TestRGBToYUVConverter(unittest.TestCase):

    def testGeneratesCorrectYUVValues(self):
        # create a pink RGB pixel
        pixel = [236, 30, 98]

        # get the YUV pixel
        yuv_pixel = rgb_to_yuv(pixel)

        # use numpy to assert the values of the pixel
        np.testing.assert_array_almost_equal(yuv_pixel, [99, 127, 225], decimal=3)


if __name__ == '__main__':
    unittest.main()

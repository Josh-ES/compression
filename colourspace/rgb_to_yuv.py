import numpy as np


def rgb_to_yuv(pixel):
    # rgb_to_yuv(pixel) converts the RGB colour values R, G and B into
    # the YUV colour space, and returns an equivalent Y, U and V
    # 3x1 array

    # first convert to numpy array
    pixel = np.array(pixel)

    if np.shape(pixel) != (3,):
        raise ValueError('The input value is not a pixel, which should be a 1D array of length 3')

    # turn pixel into a column vector, we don't need to normalise by 255
    # because we are looking to output in 8-bit integer format
    pixel = pixel.reshape(pixel.size, 1)

    # setup the transformation matrix from rgb to yuv
    yuv_transformation_matrix = np.matrix([
        [0.299, 0.587, 0.114],
        [-0.1687, -0.3313, 0.5],
        [0.5, -0.4187, -0.0813],
    ])

    # generate the YUV pixel by matrix multiplication
    yuv_pixel = yuv_transformation_matrix * pixel

    # turn yuv_pixel back into a row vector
    yuv_pixel = yuv_pixel.reshape(yuv_pixel.size,)

    # add the offsets to the pixel
    yuv_pixel += np.array([0, 128, 128])

    # round each component
    yuv_pixel = yuv_pixel.round()

    # turn yuv pixel into a list
    yuv_pixel = yuv_pixel.tolist()[0]

    # return the YUV pixel
    return yuv_pixel

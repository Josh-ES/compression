import unittest
import numpy as np
from .yuv_to_rgb import yuv_to_rgb


class TestYUVToRGBConverter(unittest.TestCase):

    def testGeneratesCorrectRGBValues(self):
        # create a pink YUV pixel
        pixel = [99, 127, 225]

        # get the RGB pixel
        rgb_pixel = yuv_to_rgb(pixel)

        # use numpy to assert the values of the pixel (note there are inaccuracies due
        # to rounding errors introduced at each step)
        np.testing.assert_array_almost_equal(rgb_pixel, [235, 30, 97], decimal=3)


if __name__ == '__main__':
    unittest.main()

#!/usr/bin/env python3

from sys import argv
from binary import coded_bytes_to_bits
from jpeg import unjpeg, save_image


def camunjpeg(filename):
    if '.camjpeg' not in filename:
        raise ValueError('The filename you passed in is not a CamJPEG file')

    file = open(filename, 'rb')
    ciphertext = file.read()
    file.close()
    file_bytes = []

    for data in ciphertext:
        file_bytes.append(data)

    file_bin = coded_bytes_to_bits(file_bytes)

    file = open(filename.replace('.camjpeg', '.camcode'), 'r')
    width, height = [int(n) for n in file.readline().split(' ')]
    codes = [line.rstrip() for line in file.readlines()]
    file.close()

    # decode the document, using the inflate method
    image = unjpeg((width, height), codes, file_bin)

    # write to an output image
    save_image(filename.replace('.camjpeg', '.output.pbm'), image)


# read the filename from the arguments to the function
f = argv[1]
camunjpeg(f)

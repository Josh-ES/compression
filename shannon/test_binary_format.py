import unittest
from shannon import binary_format


class TestBinaryFormatterWorks(unittest.TestCase):

    def testBinaryFormatterWorks(self):
        codeword = binary_format(0, 2)
        self.assertEqual(codeword, '00')

if __name__ == '__main__':
    unittest.main()
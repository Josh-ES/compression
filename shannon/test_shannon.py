import unittest
from shannon import shannon


class TestShannonWorksCorrectly(unittest.TestCase):

    def testShannonWorksCorrectly(self):
        probabilities = [0.15, 0.07, 0.17, 0.06, 0.06, 0.31, 0.18]
        codes = shannon(probabilities)
        self.assertEqual(len(codes), 7)
        self.assertEqual(codes[0], '010')
        self.assertEqual(codes[5], '00')
        self.assertEqual(codes[3], '11100')

    def testShannonWorksWithZeroProbabilities(self):
        probabilities = [0.15, 0.07, 0, 0.17, 0.06, 0.06, 0.31, 0.18]
        codes = shannon(probabilities)
        self.assertEqual(codes, ['010', '1100', '', '011', '11100', '11110', '00', '101'])

if __name__ == '__main__':
    unittest.main()
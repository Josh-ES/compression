from util import codeword_lengths, cumulate, shift_code_order
from .binary_format import binary_format


def shannon(p):
    ##############################
    # OPTION 2: SHANNON'S METHOD #
    ##############################
    
    # 1) COMPUTE THE CUMULATIVE DISTRIBUTION f FROM p
    # 2) CONVERT THE CUMULATIVE PROBABILITIES TO BINARY 
    # 3) TRUNCATE RESULTING CODE TABLE TO LENGTH OF LONGEST CODEWORD

    # first count the number of zeros we have
    num_zeros = sum([n == 0 for n in p])

    # compute the cumulative distribution f from p
    sorted_probabilities = sorted([n for n in p if not n == 0], reverse=True)
    lengths = codeword_lengths(sorted_probabilities)
    cumsum = cumulate(sorted_probabilities)

    # convert the cumulative probabilities to binary and truncate, and pad with empty codewords for each zero
    codes = [''] * num_zeros + [binary_format(val, lengths[key]) for key, val in enumerate(cumsum)]

    return shift_code_order(codes, p)

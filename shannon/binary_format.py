from math import floor


def binary_format(number, length):
    floored = floor(number * 2 ** length)
    to_represent = int(floored)
    # return the integer in binary form, with zeros padded so string is of length = length
    return format(to_represent, '0{0}b'.format(length))

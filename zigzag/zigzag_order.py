import numpy as np
from .zigzag import zigzag


def zigzag_order(square_matrix):
    # zigzag_order(square_matrix) takes a square matrix as an argument, and
    # returns a 1-dimensional list with the elements of the square matrix
    # arranged in "zigzag" order, as required during JPEG encoding
    #
    # Copyright Joshua 2017

    shape = np.shape(square_matrix)

    if shape[0] != shape[1]:
        raise ValueError('The matrix passed as an argument is not square')

    # get the coordinates in traversal order, which will come back in the
    # tuple form (row, column)
    coordinates = zigzag(shape[0])

    # initialise a list of elements to return
    elements = []

    # traverse the square matrix in zigzag order
    for coord in coordinates:
        elements.append(square_matrix[coord])

    return elements

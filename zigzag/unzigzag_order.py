from math import sqrt
import numpy as np
from .zigzag import zigzag


def unzigzag_order(elements):
    # unzigzag_order(elements) takes a series of elements as an argument, and
    # returns a square matrix with the elements of the square matrix being the
    # same elements as passed in the argument, arranged on the assumption
    # that the passed elements are in zigzag order
    #
    # Copyright Joshua 2017

    n_elements = len(elements)
    dimension = round(sqrt(n_elements))

    if dimension - sqrt(n_elements) > 0:
        raise ValueError('For the elements to be zigzag ordered, there must be a square number of them')

    # get the coordinates in traversal order, which will come back in the
    # tuple form (row, column)
    coordinates = zigzag(dimension)

    # initialise a list of elements to return
    matrix = np.zeros((dimension, dimension))

    # traverse the square matrix in zigzag order
    for i, coord in enumerate(coordinates):
        matrix[coord] = elements[i]

    return matrix

import unittest
import numpy as np
from .unzigzag_order import unzigzag_order


class TestZigzagUnOrderer(unittest.TestCase):

    def testOutputsCorrectIndicesOrder(self):
        expected_matrix = np.matrix([
            [-26, -3, -6, 2, 2, -1, 0, 0],
            [0, -2, -4, 1, 1, 0, 0, 0],
            [-3, 1, 5, -1, -1, 0, 0, 0],
            [-3, 1, 2, -1, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
        ])

        elements = [-26, -3, 0, -3, -2, -6, 2, -4, 1, -3, 1, 1, 5, 1, 2, -1, 1, -1, 2, 0, 0, 0, 0, 0, -1, -1, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0]

        # get the elements of the square matrix in zigzag order
        square_matrix = unzigzag_order(elements)

        # assert that the resulting matrix is the same
        np.testing.assert_array_equal(square_matrix, expected_matrix)


if __name__ == '__main__':
    unittest.main()

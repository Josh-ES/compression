def zigzag(n):
    # To encode each 8x8 block for the JPEG file, the values within the block
    # must be ordered in a "zigzag" order. zigzag(n) generates a list of coordinates
    # that must be traversed sequentially in order to traverse an nxn matrix
    # in "zigzag" order.
    #
    # The coordinates are returned in (row, column) format so they can be easily
    # followed when traversing a matrix
    #
    # Copyright Joshua 2017

    all_coordinates = [(x, y) for x in range(n) for y in range(n)]

    # the sum x + y is constant along each diagonal, but if the sum is odd we
    # go down the diagonal and if the sum is even we go up the diagonal, so
    # we do two sorts
    ordered_coordinates = sorted(all_coordinates,
                                 key=lambda p: (p[0] + p[1], p[1] if (p[0] + p[1]) % 2 == 0 else -p[1]))

    return ordered_coordinates

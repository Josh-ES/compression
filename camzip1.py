#!/usr/bin/env python3

import numpy as np
from fano import fano
from encode import vl_encode
from binary import bits_to_coded_bytes
from stats import entropy
from sys import argv


def camzip1(filename):
    file = open(filename, 'rb')
    file_input = file.read()
    file.close()

    # the sequence of integers used as bins for the histogram define the edges of the bins, so we need to
    # include 256. values are placed into bins according to a <= value < b, where a and b are the edges
    # of the relevant bin
    hist, bin_edges = np.histogram([data for data in file_input], bins=range(0, 256 + 1))
    total = np.sum(hist)
    p = [n / total for n in hist]

    codes = fano(p)
    hamlet_bin = vl_encode(file_input, codes)

    hamlet_cz1 = bits_to_coded_bytes(hamlet_bin)
    print('Actual Compression Performance:', len(hamlet_cz1) / len(file_input) * 8, 'bits per byte')
    print('Theoretical Compression Performance:', entropy(p), 'bits per byte')

    # write the binary code to a file
    file = open(filename + '.cz1', 'wb')
    file.write(bytearray(hamlet_cz1))
    file.close()

    # write the code table to a file
    file = open(filename + '.cz1c', 'w')

    for cwd in codes:
        file.write("%s\n" % cwd)

    file.close()

# read the filename from the arguments to the function
filename = argv[1]
camzip1(filename)

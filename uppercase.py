#!/usr/bin/env python3

from sys import argv


def uppercase(filename):
    file = open(filename, 'r')
    file_input = file.read()
    file.close()

    # write the code table to a file
    file = open(filename, 'w')
    file.write(file_input.upper())
    file.close()

# read the filename from the arguments to the function
filename = argv[1]
uppercase(filename)

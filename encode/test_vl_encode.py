import unittest
from .vl_encode import vl_encode


class TestEncoderWorks(unittest.TestCase):

    def testCorrectAlphabetProduced(self):
        encoded = vl_encode('aaabbbcdefggg', ['00', '01', '1100', '1101', '1110', '1111', '10'], alphabet=['a', 'b', 'c', 'd', 'e', 'f', 'g'])
        reverse_encoded = vl_encode('gggfedcbbbaaa', ['00', '01', '1100', '1101', '1110', '1111', '10'], alphabet=['a', 'b', 'c', 'd', 'e', 'f', 'g'])
        self.assertEqual(len(encoded), len(reverse_encoded))
        self.assertEqual(encoded[0:6], '000000')
        self.assertEqual(encoded[0:6], reverse_encoded[-6:])
        self.assertEqual(encoded[6:12], '010101')
        self.assertEqual(encoded[6:12], reverse_encoded[-12:-6])

if __name__ == '__main__':
    unittest.main()

from xtree import code_to_xtree
from tree import get_leaves, is_leaf


def vl_decode(input, codes, alphabet=None, bytes=False):
    for bit in set(input):
        if bit not in ['0', '1']:
            raise ValueError('The input string is not a binary sequence')

    tr = code_to_xtree(codes)
    leaves = [node.value for node in get_leaves(tr)]
    sorted_leaves = sorted(leaves)
    mapping = [sorted_leaves.index(val) for val in leaves]

    if alphabet is None:
        if bytes:
            alphabet = range(0, len(codes))
        else:
            alphabet = [chr(i) for i in range(0, len(codes))]

    alphabet = [alphabet[index] for index, cwd in enumerate(codes) if len(cwd) > 0]

    if not len(leaves) == len(alphabet):
        raise ValueError('Alphabet size does not match number of leaves in tree')

    # decode input vector
    node = tr

    if bytes:
        plaintext = []
    else:
        plaintext = ''

    for char in input:
        if char == '0' and node.left is not None:
            # if the char is zero, move to the left on the tree
            node = node.left

        if char == '1' and node.right is not None:
            # if the char is one, move to the right on the tree
            node = node.right

        if is_leaf(node):
            # convert the leaf node value to an index in the alphabet
            index_in_alphabet = mapping[leaves.index(node.value)]

            if bytes:
                plaintext.append(alphabet[index_in_alphabet])
            else:
                plaintext += alphabet[index_in_alphabet]

            # reset the current node to the root again
            node = tr

    return plaintext

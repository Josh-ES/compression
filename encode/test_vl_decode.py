import unittest
from .vl_decode import vl_decode


class TestDecoderWorks(unittest.TestCase):

    def testCorrectTextProduced(self):
        original_text = vl_decode('0000000101011100110111101111101010',
                                  ['00', '01', '1100', '1101', '1110', '1111', '10'],
                                  ['a', 'b', 'c', 'd', 'e', 'f', 'g'])

        self.assertEqual(original_text, 'aaabbbcdefggg')

    def testReversedTextCorrect(self):
        reversed_text = vl_decode('1010101111111011011100010101000000',
                                  ['00', '01', '1100', '1101', '1110', '1111', '10'],
                                  ['a', 'b', 'c', 'd', 'e', 'f', 'g'])

        self.assertEqual(reversed_text, 'gggfedcbbbaaa')

if __name__ == '__main__':
    unittest.main()

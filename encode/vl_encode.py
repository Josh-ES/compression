from util import setdiff


def vl_encode(input, codes, alphabet=None):
    # vl_encode(x, c, cl) encodes the input word x using the variable length
    # code described in c (every row contains a codeword) and cl (lengths of
    # the codewords.) The alphabet is assumed to be the set of distinct
    # elements in x sorted in increasing order and the code size is checked
    # for consistency with this alphabet. Alternatively, the alphabet can be
    # supplied as an optional extra argument and if so, its size can exceed the
    # number of distinct symbols in x (e.g. if there are codewords for symbols
    # that don't occur in x and hence won't be used in this encoding run.)
    #
    # Copyright Joshua, 2017

    if alphabet is not None:
        if len(setdiff(set(input), alphabet)) > 0:
            raise ValueError('There are symbols in data that are not in alphabet.')
    else:
        alphabet = range(0, len(codes))

    asize = len(alphabet)

    if asize > len(codes):
        raise ValueError('Input word alphabet inconsistent with code dimension.')

    # binary encode
    y = ''

    for x in input:
        pos_in_alphabet = alphabet.index(x)
        y += codes[pos_in_alphabet]

    return y

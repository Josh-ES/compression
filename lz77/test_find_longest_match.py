import unittest
from .find_longest_match import find_longest_match


class TestLZ77MatchFinderCorrect(unittest.TestCase):

    def testReturnsMatchTuple(self):
        match = find_longest_match('ABBABBABBBAABABA', 3, 4)
        self.assertEqual(match, (1, 3, 6))

    def testReturnsUncompressedChar(self):
        match = find_longest_match('ABBABBABBBAABABA', 1, 4)
        self.assertEqual(match, (0, 'B', None))

    def testWindowLengthWorksCorrectly(self):
        match = find_longest_match('ABBABBABBBAABABA', 11, 4)
        self.assertEqual(match, (0, 'A', None))


if __name__ == '__main__':
    unittest.main()

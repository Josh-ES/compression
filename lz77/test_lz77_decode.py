import unittest
from .lz77_decode import lz77_decode


class TestLZ77DecoderWorking(unittest.TestCase):

    def testCorrectStringDecoded(self):
        plaintext = lz77_decode('00100000100100001000100001010110110110000100010000011011001010100010', 4)
        self.assertEqual(plaintext, 'ABBABBABBBAABABA')

    def testCorrectStringDecodedWithDefaultWindow(self):
        plaintext = lz77_decode('001000001001000010001000010100000000001101101000000000111001010000000'
                                '010110010100000000001000110')

        self.assertEqual(plaintext, 'ABBABBABBBAABABA')


if __name__ == '__main__':
    unittest.main()

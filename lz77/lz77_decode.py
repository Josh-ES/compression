from math import floor, log

UNCOMPRESSED_FLAG = 0
MATCH_FLAG = 1


def lz77_decode(ciphertext, w=4095, lookahead=15):
    # lz77_decode(ciphertext) implements the LZ77 decoder for the ciphertext
    # sequence x. Returns the plaintext that was originally compressed.
    #
    # Copyright Joshua 2017

    pointer_bits = floor(log(w, 2) + 1)
    length_bits = floor(log(lookahead, 2) + 1)

    # initialise iteration pointer and the plaintext sequence string
    i = 0
    plaintext = ''

    # check that i doesn't hit the end of the ciphertext, noting that we may have trailing zeros
    # to contend with
    while i < len(ciphertext) - min(8, int(pointer_bits + length_bits)):
        flag = ciphertext[i]

        if flag == str(UNCOMPRESSED_FLAG):
            # add the uncompressed character to the plaintext
            binary_char = ciphertext[i + 1:i + 9]
            plaintext += chr(int(binary_char, 2))

            # increase pointer by 9
            i += 9
        elif flag == str(MATCH_FLAG):
            # get the location and length of the repeat match
            location = len(plaintext) - int(ciphertext[i + 1:i + 1 + pointer_bits], 2)
            length = int(ciphertext[i + pointer_bits + 1:i + pointer_bits + 1 + length_bits], 2)

            # iterate through the plain text string, copying characters one by one. This approach
            # is necessary since the match might include the new string
            for j in range(location, location + length):
                plaintext += plaintext[j]

            # increase pointer by 1 for the flag, pointer bits and length bits
            i += 1 + pointer_bits + length_bits

    return plaintext

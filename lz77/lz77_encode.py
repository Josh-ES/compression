from console import print_progress_bar
from math import floor, log
from sys import stdout
from .find_longest_match import find_longest_match

UNCOMPRESSED_FLAG = 0
MATCH_FLAG = 1


# set window size to 12 bits and lookahead to 4 bits
def lz77_encode(x, w=4095, lookahead=15):
    # lz77_encode(x) implements the LZ77 encoder for the source
    # sequence x. LZ77 compresses by replacing repeated occurrences of data with
    # references to a copy existing earlier.
    #
    # To find matches, the encoder keeps track of some amount of the most recent
    # data encountered. The structure in which this is held is called a sliding window.
    # This LZ77 implementation is therefore sliding-window compression.
    #
    # Actual implementation is LZSS, which accounts better for cases where there is
    # no match (see FTR).
    #
    # We need a number of bits to store the length of matches, and a
    # number of bits to store the pointer to the match, which could be up to or equal to
    # the window size. Choose 4 bits and 12 bits respectively, arbitrarily, as these
    # seem to be common in other implementations.
    # TODO check these numbers are reasonable
    #
    # Copyright Joshua 2017

    i = 0
    data = ''
    print_progress_bar(0, len(x), prefix='Progress:', suffix='Complete', length=80)

    while i < len(x):
        flag, pointer_or_match, length = find_longest_match(x, i, w, lookahead)

        if flag == MATCH_FLAG:
            i += length

            # add the flag bit
            data += str(MATCH_FLAG)

            # find the number of bits required for the window, and add the pointer
            pointer_bits = floor(log(w, 2) + 1)
            data += ('{0:0' + str(pointer_bits) + 'b}').format(pointer_or_match)

            # find the number of bits required for the lookahead, and add the length
            length_bits = floor(log(lookahead, 2) + 1)
            data += ('{0:0' + str(length_bits) + 'b}').format(length)
        elif flag == UNCOMPRESSED_FLAG:
            i += 1

            # add the flag bit
            data += str(UNCOMPRESSED_FLAG)

            # check if the character is passed in as a byte or as a character
            if type(pointer_or_match) == str:
                match = ord(pointer_or_match)
            else:
                match = pointer_or_match

            # add the literal character as an 8 bit binary string
            data += '{0:08b}'.format(match)

        print_progress_bar(i + 1, len(x), prefix='Progress:', suffix='Complete', length=80)

    stdout.write('\n\n')

    return data

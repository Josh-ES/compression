import unittest
from .lz77_encode import lz77_encode


class TestLZ77EncoderWorking(unittest.TestCase):

    def testCorrectGroupsReturned(self):
        cipher = lz77_encode('ABBABBABBBAABABA', 4)
        self.assertEqual(cipher, '00100000100100001000100001010110110110000100010000011011001010100010')


if __name__ == '__main__':
    unittest.main()

def find_longest_match(data, i, w=4095, lookahead=15):
    # find_longest_match(data, i) finds the longest match in the previously
    # read string.

    # we must account for the size of string to match. This is usually just
    # the size of the window w, but it's possible that i, the position we are
    # currently at, is within w of the end of the data.
    end_of_window = min(len(data), i + lookahead)

    # we are looking for the longest length, so let's start from the maximum
    # length of string and work down. Iterate using a pointer to the end of
    # the substring, so this replaces k in reference [1]. We want matches of
    # length two or more
    for end_pointer in range(end_of_window, i + 1, -1):
        substring = data[i:end_pointer]

        # find the value of k, the length of the match
        k = end_pointer - i

        # we need to find the start of the window, which is usually just
        # i - 1 - w, where i is the current position. However, this value
        # can in some cases be less than zero, the start index
        start_of_window = max(i - w, 0)

        # now iterate through the sliding window to find a match. We need to
        # iterate up to the current position, but we need to consider when
        # length of the desired match k. The target overlaps with new phrase,
        # so match could be arbitrarily long, however, the match length restricts
        # this. e.g. BCBCBCAAAAA
        for j in range(start_of_window, i):
            match_string = data[j:j+k]

            if match_string == substring:
                # return a tuple (flag, match_pointer, match_length)
                # use a 1 flag bit for a repeated match. The match pointer is
                # measured backward from the current position i to the start
                # of the match
                return 1, i - j, k

    # no match found? return the uncompressed character with a 0 flag bit
    # return None as the third value so we're consistent in return type
    return 0, data[i], None

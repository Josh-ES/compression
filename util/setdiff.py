def setdiff(a, b):
    # This method returns the data in A that is not in B, with no
    #  repetitions and in sorted order.

    li = list(set(a) - set(b))
    return sorted(li)

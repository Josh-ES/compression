def cumulate(lis):
    return list(cumulate_generator(lis))


def cumulate_generator(lis):
    total = 0

    for x in lis:
        yield total
        total += x

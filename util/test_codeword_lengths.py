import unittest
from util import codeword_lengths


class TestCodewordLengthsGenerator(unittest.TestCase):

    def testCorrectLengthsReturned(self):
        probabilities = [0.15, 0.07, 0.17, 0.06, 0.06, 0.31, 0.18]
        lengths = codeword_lengths(probabilities)
        self.assertEqual(lengths, [3, 4, 3, 5, 5, 2, 3])

    def testHandleZeroCases(self):
        probabilities = [0.0, 0.0, 0.0, 0.15, 0.07, 0.17, 0.06, 0.06, 0.31, 0.18]
        lengths = codeword_lengths(probabilities)
        self.assertEqual(lengths, [0, 0, 0, 3, 4, 3, 5, 5, 2, 3])

if __name__ == '__main__':
    unittest.main()
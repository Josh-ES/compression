from .codeword_lengths import codeword_lengths


def shift_code_order(codes, p):
    # order the codes so they are in the same order as the original probabilities vector
    lengths = codeword_lengths(p)
    # sort the lengths by value, maintaining their original indices
    sorted_lengths = sorted(enumerate(lengths), key=lambda x: x[1])
    # create an array of tuples (value in ordered array to go to, value in codes to take from)
    order_mapping = [(value[0], key) for key, value in enumerate(sorted_lengths)]
    # order the codes based on the mapping
    ordered_codes = [codes[n[1]] if n[1] < len(codes) else '' for n in sorted(order_mapping, key=lambda x: x[0])]

    return ordered_codes

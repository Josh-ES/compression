from math import ceil, log


def codeword_lengths(p):
    log_values = [log(1 / n, 2) if not n == 0 else 0 for n in p]
    return [ceil(n) for n in log_values]

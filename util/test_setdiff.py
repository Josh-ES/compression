import unittest
from util import setdiff


class TestSetDiffWorksCorrectly(unittest.TestCase):

    def testCorrectDifferenceCalculated(self):
        setA = ['a', 'c', 'e', 'g']
        setB = ['a', 'b', 'e', 'f']
        diff = setdiff(setA, setB)
        self.assertEqual(diff, ['c', 'g'])

if __name__ == '__main__':
    unittest.main()
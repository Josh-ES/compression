import unittest
from util import cumulate


class TestCumulatorWorksCorrectly(unittest.TestCase):

    def testCumulativeSumsCalculated(self):
        ls = [4, 6, 10]
        cumsum = cumulate(ls)
        self.assertEqual(cumsum, [0, 4, 10])

if __name__ == '__main__':
    unittest.main()
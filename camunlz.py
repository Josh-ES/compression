#!/usr/bin/env python3

from sys import argv
from binary import coded_bytes_to_bits
from lz77 import lz77_decode


def camunlz(filename):
    if '.clz' not in filename:
        raise ValueError('The filename you passed in is not a CamLz file')

    file = open(filename, 'rb')
    ciphertext = file.read()
    file.close()

    file_bytes = []

    for data in ciphertext:
        file_bytes.append(data)

    file_bin = coded_bytes_to_bits(file_bytes)

    # decode the document
    plaintext = lz77_decode(file_bin)

    # write to the output file in binary
    file = open(filename.replace('.clz', '.ulz'), 'wb')
    b = bytearray()
    b.extend(map(ord, plaintext))
    file.write(b)
    file.close()


# read the filename from the arguments to the function
f = argv[1]
camunlz(f)

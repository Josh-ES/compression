from math import ceil, floor
import numpy as np
from util import cumulate


def decode(data, alphabet=None):
    # adaptive.encode(plaintext, alphabet) implements a context adaptive binary arithmetic decoder
    # for the input data. The probability distribution is assumed uniform initially, i.e. the
    # frequency of each character is assumed to equal 1 initially.
    #
    # Copyright Joshua 2017

    # define the integer versions of 1.0, 0.5, 0.25 and 0.75

    # 32 bit integer values
    precision = 32

    # this is the max_integer
    one = (2 ** precision) - 1
    quarter = ceil(one / 4)
    half = 2 * quarter
    three_quarters = 3 * quarter

    if alphabet is None:
        # if we don't have an alphabet, assume full ascii alphabet
        alphabet = [chr(val) for val in range(0, 256)] + ['EOF']
    else:
        # otherwise, take the alphabet as is and add an end of file character
        alphabet += ['EOF']

    # set up a uniform frequency distribution as the initial condition
    frequencies = np.ones(len(alphabet))

    # initialise the output string
    y = ''

    # initialise the interval endpoints
    hi = one
    lo = 0

    # add dummy zeros to the input row vector
    for n in range(0, precision):
        data += '0'

    # get the integer value of the string
    value = int(data[0:precision], 2)
    xp = precision

    while True:
        # generate the new probability distribution and cumulative probability distribution
        probabilities = frequencies / sum(frequencies)
        f = cumulate(probabilities)

        width = hi - lo + 1
        ind = max([index for index, n in enumerate(f) if lo + ceil(n*width) <= value])
        char = alphabet[ind]

        # if we encounter the end of file indicator, break the loop and return
        if char == 'EOF':
            break

        y += alphabet[ind]

        # set the new intervals
        lo += ceil(f[ind] * width)
        hi = lo + floor(probabilities[ind] * width)

        # check that the interval has not narrowed to 0
        if hi == lo:
            raise ValueError('Interval has become zero: check that you have no zero probs and increase precision')

        while True:
            if hi < half:
                # do nothing
                pass
            elif lo >= half:
                lo -= half
                hi -= half
                value -= half
            elif (lo >= quarter) and (hi < three_quarters):
                # interval within [1/4, 3/4)
                lo -= quarter
                hi -= quarter
                value -= quarter
            else:
                break

            lo *= 2
            hi = hi * 2 + 1
            value = value * 2 + int(data[xp], 2)
            xp += 1

            if xp == len(data) + 1:
                raise Exception('Unable to decompress')

        # after the while loop, we must add one to the relevant frequency to update the
        # probability distribution
        frequencies[ind] += 1

    return y

from math import ceil, floor
import numpy as np
from util import cumulate


def encode(plaintext, alphabet=None):
    # adaptive.encode(plaintext, alphabet) implements a context adaptive binary arithmetic encoder
    # for the input plaintext.
    #
    # Copyright Joshua 2017

    # define the integer versions of 1.0, 0.5, 0.25 and 0.75

    # 32 bit integer values
    precision = 32

    # this is the max_integer
    one = 2 ** precision - 1
    quarter = ceil(one/4)
    half = 2 * quarter
    three_quarters = 3 * quarter

    if alphabet is None:
        # if we don't have an alphabet, assume full ascii alphabet
        alphabet = [val for val in range(0, 256)] + ['EOF']
    else:
        # otherwise, take the alphabet as is and add an end of file character
        alphabet += ['EOF']

    # set up a uniform frequency distribution as the initial condition
    frequencies = np.ones(len(alphabet))

    # initialise the output string, interval endpoints and straddle counter
    y = ''
    lo = 0
    hi = one
    straddle = 0

    # set plaintext to a list version of itself
    plaintext = list(plaintext) + ['EOF']

    # MAIN ENCODING ROUTINE
    for char in plaintext:
        # generate the new probability distribution and cumulative probability distribution
        probabilities = frequencies / sum(frequencies)
        f = cumulate(probabilities)
    
        # calculate the interval range to be the difference between hi and lo + 1
        # The +1 is necessary to avoid rounding issues
        width = hi - lo + 1

        # The following command finds the index of the next input symbol in
        # the cumulative probability distribution f
        ind = alphabet.index(char)

        # narrow the interval end-points [lo,hi) to the new range [f,f+p] within the old
        # interval [lo,hi], rounding 'inwards' so the code remains prefix-free
        lo += ceil(f[ind] * width)
        hi = lo + floor(probabilities[ind] * width)

        # check that the interval has not narrowed to 0
        if hi == lo:
            raise ValueError('Interval has become zero: check that you have no zero' +
                             ' probabilities and increase precision')

        # rescale the interval if its end-points have bits in
        # common, and output the corresponding bits where appropriate
        while True:
            if hi < half:
                # lo < hi < 1/2 -> stretch interval and emit 0
                # append a 0 followed by 'straddle' ones to the output
                # string. Reset the straddle counter to zero
                y += '0'

                for n in range(0, straddle):
                    y += '1'

                straddle = 0

            elif lo >= half:
                # hi > lo >= 1/2 -> stretch and emit 1
                # append a 1 followed by 'straddle' zeros to the output
                # string and reset the straddle counter
                y += '1'

                for n in range(0, straddle):
                    y += '0'

                straddle = 0

                # take integer 'half' from lo and hi (the actual interval
                # re-stretching will follow after the if statement.
                #
                # in this operation, we want to multiply the endpoints by two
                # and shift them back to the [0, 1) range by subtracting 1
                lo -= half
                hi -= half

            elif (lo >= quarter) and (hi < three_quarters):
                # interval within [1/4, 3/4]
                # take integer 'quarter' from lo and hi to prepare for a
                # centered re-stretch, and increase the 'straddle' counter by
                # one.
                lo -= quarter
                hi -= quarter
                straddle += 1

            else:
                # the interval is now stretched to the point where it does not
                # have bits in agreement and we can break the endless loop
                break

            # now multiply the interval end-points by 2 (the -1/2 or -1/4
            # operations have already been performed if appropriate, so the
            # interval is now in all cases within [0,1/2] and can simply be
            # multiplied by 2 to stretch to [0,1]. ADD 1 TO THE HI END-POINT
            # AFTER MULTIPLYING. THIS ENSURES THAT A 1 BIT IS PIPELINED INTO
            # THE HI BOUND AND WILL HELP AVOID UNDERFLOW/OVERFLOW.

            lo *= 2
            hi = hi * 2 + 1

        # after the while loop, we must add one to the relevant frequency to update the
        # probability distribution
        frequencies[ind] += 1

    # Add termination bits to the output string to ensure that the final
    # dyadic interval lies within the source interval
    straddle += 1

    if lo < quarter:
        y += '0'

        for n in range(0, straddle):
            y += '1'
    else:
        y += '1'

        for n in range(0, straddle):
            y += '0'

    return y

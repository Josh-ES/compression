import unittest
from .encode import encode


class TestAdaptiveArithmeticCodingWorking(unittest.TestCase):

    def testCorrectCodesReturned(self):
        ciphertext = encode('A', ['A', 'B', 'C'])
        self.assertEqual(ciphertext, '00111')


if __name__ == '__main__':
    unittest.main()

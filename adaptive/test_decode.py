import unittest
from .decode import decode


class TestAdaptiveArithmeticDecoder(unittest.TestCase):

    def testCorrectCodesReturned(self):
        plaintext = decode('00111', ['A', 'B', 'C'])
        self.assertEqual(plaintext, 'A')


if __name__ == '__main__':
    unittest.main()

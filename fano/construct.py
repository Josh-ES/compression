from binarytree import Node
from util import codeword_lengths


def construct(p):
    # This internal Shannon-Fano function assumes that its input probability
    # vector is sorted in decreasing order and doesn't contain any zero
    # probabilities. This is the Fano method implementation.
    #
    # Copyright Joshua 2017
    
    # compute the codeword lengths for the Shannon-Fano code
    lengths = codeword_lengths(p)

    # set up a root node
    root = Node(1)

    # set up values of interest
    index = 2
    current_parent = root
    current_depth = 0

    for key_value in sorted(enumerate(lengths), key=lambda x: x[1]):
        # iterate over each length and construct a leaf at that depth each time
        letter = chr(ord('A') + key_value[0])
        target_depth = key_value[1]

        if not target_depth == 0:
            [current_parent, current_depth, index] = construct_leaf(current_parent,
                                                                    current_depth,
                                                                    target_depth,
                                                                    index,
                                                                    letter)

    return root


def construct_leaf(node, current_depth, target_depth, index, identifier):
    # if we have reached the target depth, construct the node and return
    if current_depth == target_depth:
        node.value = identifier
        return [node.parent, current_depth - 1, index - 1]

    # if we haven't reached the target depth, start by creating a node to the left
    if node.left is None:
        node.left = Node(index)
        node.left.parent = node
        return construct_leaf(node.left, current_depth + 1, target_depth, index + 1, identifier)

    # if a node has already been created to the left, try right instead
    elif node.right is None:
        node.right = Node(index)
        node.right.parent = node
        return construct_leaf(node.right, current_depth + 1, target_depth, index + 1, identifier)

    # if both child slots are occupied, move up to the parent and try again
    else:
        return construct_leaf(node.parent, current_depth - 1, target_depth, index, identifier)


from .construct import construct
from xtree import xtree_to_code
from util import shift_code_order


def fano(p):
    ############################
    # OPTION 1: CONSTRUCT TREE #
    ############################
    
    # 1) start with a tree xt consisting only of a root (a column of 3 zeros)
    # 2) initialise a variable to hold the "current" node (initially 1), the
    # current tree depth (initially zero), and the next codeword that needs to
    # be assigned a leaf (initially 1)
    # 3) start an infinite loop (this will be broken once the last codeword is
    # assigned
    # 4) check if the current node has any unassigned (zero) children
    # 5) if not, move one level up the tree (don't forget to update the node
    # AND the tree depth) and use the command "continue" to return to the start
    # of the infinite loop
    # 6) pick the first unassigned child, create a new node at the end of the
    # tree and assign that node as a new child and the current node as its
    # parent, then move to the new node (not forgetting to update the depth)
    # 7) check if there is a need for a codeword whose length is the new depth
    # 8) if there is AND it is the LAST codeword that needed assigning, break
    # the infinite loop
    # 9) otherwise, if there is a need for a codeword at this depth, simply
    # crawl back up to the current node's parent. The node you've just left
    # will never be visited again and will hence become a leaf. Update the
    # codeword counter.
    # 10) convert from xt to a code table

    # first count the number of zeros we have
    num_zeros = sum([n == 0 for n in p])

    # construct the tree
    tr = construct(p)

    # pad the codewords array with empty codewords for each zero
    codes = [''] * num_zeros + xtree_to_code(tr)

    # shift the codes as normal
    return shift_code_order(codes, p)


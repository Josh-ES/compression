import unittest
from fano import fano


class TestFanoCodesCorrect(unittest.TestCase):

    def testCorrectFanoCodewords(self):
        probabilities = [0.15, 0.07, 0.17, 0.06, 0.06, 0.31, 0.18]
        codes = fano(probabilities)
        self.assertEqual(len(codes), 7)
        self.assertEqual(codes[0], '010')
        self.assertEqual(codes[5], '00')
        self.assertEqual(codes[3], '10110')

    def testFanoCodewordsWithZeroProbabilities(self):
        probabilities = [0.15, 0.07, 0, 0.17, 0.06, 0.06, 0.31, 0.18]
        codes = fano(probabilities)
        self.assertEqual(codes, ['010', '1010', '', '011', '10110', '10111', '00', '100'])

if __name__ == '__main__':
    unittest.main()

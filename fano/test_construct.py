import unittest
from fano import construct
from numpy import random
from tree import count_leaves


class TestFanoMethodImplementation(unittest.TestCase):

    def testCorrectTreeProduced(self):
        probabilities = [0.15, 0.07, 0.17, 0.06, 0.06, 0.31, 0.18]
        tr = construct(probabilities)
        self.assertEqual(tr.left.left.value, 'F')
        self.assertEqual(tr.left.right.left.value, 'A')
        self.assertEqual(tr.left.right.right.value, 'C')

    def testAgainstRandomDistribution(self):
        rand_vector = random.rand(10)
        total = sum(rand_vector)
        probabilities = [p / total for p in rand_vector]
        tr = construct(probabilities)
        self.assertEqual(count_leaves(tr), 10)

if __name__ == '__main__':
    unittest.main()
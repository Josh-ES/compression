#!/usr/bin/env python3

from sys import argv
from binary import coded_bytes_to_bits
from deflate import inflate


def caminflate(filename):
    if '.def' not in filename:
        raise ValueError('The filename you passed in is not a CamDEFLATE file')

    file = open(filename, 'rb')
    ciphertext = file.read()
    file.close()
    file_bytes = []

    for data in ciphertext:
        file_bytes.append(data)

    file_bin = coded_bytes_to_bits(file_bytes)

    file = open(filename + 'c', 'r')
    codes = [line.rstrip() for line in file.readlines()]
    file.close()

    # decode the document, using the inflate method
    plaintext = inflate(file_bin, codes)

    # write to the output file in binary
    file = open(filename.replace('.def', '.output'), 'wb')
    b = bytearray()
    b.extend(map(ord, plaintext))
    file.write(b)
    file.close()


# read the filename from the arguments to the function
filename = argv[1]
caminflate(filename)

from binarytree import Node
from .construct import construct
from xtree import xtree_to_code


def huffman(p):
    nodes = []

    for index, prob in enumerate(p):
        # set the index to the length of p + the index, so our codes are in the right order eventually
        n = Node(len(p) + index)
        # create a custom field on the node object for the probability associated
        n.prob = prob
        nodes.append(n)

    # we must first sort the nodes in descending order of probability
    #
    # note contrast to notes, where the nodes are in ascending order. this approach allows use of list.pop()
    #  in python
    root = construct(nodes)

    # we need to shift the code based on the order the leaves were constructed
    return xtree_to_code(root, order_by_leaf_values=True)

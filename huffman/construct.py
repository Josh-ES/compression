from binarytree import Node


def construct(nodes):
    # get the length of the nodes, we will use it as an index later
    l = len(nodes)

    # BASE CASE if there is only one node, it is the root so return it
    if l == 1:
        return nodes[0]

    # first sort the nodes into an appropriate order
    sorted_nodes = sorted(nodes, key=lambda node: node.prob, reverse=True)

    # get the two nodes with the lowest probability
    min1 = sorted_nodes.pop()
    min2 = sorted_nodes.pop()

    # create a new node, add the probabilities together and set the children appropriately
    n = Node(l - 1)
    n.prob = min1.prob + min2.prob
    n.left = min2
    n.right = min1

    sorted_nodes.append(n)
    return construct(sorted_nodes)

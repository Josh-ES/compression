import unittest
from .huffman import huffman


class TestHuffmanCodesCorrect(unittest.TestCase):

    def testCorrectHuffmanCodewords(self):
        # supply probabilities in order so the code order corresponds to the notes
        probabilities = [0.05, 0.10, 0.15, 0.20, 0.20, 0.30]
        codes = huffman(probabilities)
        self.assertEqual(codes, ['0001', '0000', '001', '10', '11', '01'])

if __name__ == '__main__':
    unittest.main()

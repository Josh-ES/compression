import unittest
from .construct import construct
from binarytree import Node
from tree import is_leaf


class TestHuffmanTreeConstructor(unittest.TestCase):

    def testCorrectTreeProduced(self):
        # supply probabilities in order so the code order corresponds to the notes
        probabilities = [0.05, 0.10, 0.15, 0.20, 0.20, 0.30]

        nodes = []

        for index, prob in enumerate(probabilities):
            # set the index to the length of p + the index, so our codes are in the right order eventually
            n = Node(len(probabilities) + index)
            # create a custom field on the node object for the probability associated
            n.prob = prob
            nodes.append(n)

        root = construct(nodes)

        # test that each leaf is correctly constructed
        #
        # note that we expect a tree to be constructed of the
        self.assertTrue(is_leaf(root.left.left.left.left))
        self.assertTrue(is_leaf(root.left.left.left.right))
        self.assertTrue(is_leaf(root.left.left.right))
        self.assertTrue(is_leaf(root.left.right))
        self.assertTrue(is_leaf(root.right.left))
        self.assertTrue(is_leaf(root.right.right))

if __name__ == '__main__':
    unittest.main()

def block_split(image, block_size=8):
    # block_split(image) splits the image matrix into three arrays of blocks,
    # where each block is a block_size x block_size matrix. This is typically
    # an 8x8 matrix for JPEG encoding, so we will use that here

    blocks_1 = []
    blocks_2 = []
    blocks_3 = []

    # get the number of rows and columns in the image
    rows, cols = image.shape[0], image.shape[1]

    for i in range(0, rows, block_size):
        for j in range(0, cols, block_size):
            blocks_1.append(image[i:i + block_size, j:j + block_size, 0])
            blocks_2.append(image[i:i + block_size, j:j + block_size, 1])
            blocks_3.append(image[i:i + block_size, j:j + block_size, 2])

    return blocks_1, blocks_2, blocks_3

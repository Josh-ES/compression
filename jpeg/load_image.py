import cv2
import numpy as np


def load_image(filename, b=8):
    # load_image(filename) reads the file given by the argument
    # filename and returns a 3-dimensional array containing the RGB values
    # (R has z-index 0, G index 1 and B index 2)
    #
    # The method uses OpenCV, as this is the easiest Python method to
    # read images' RGB data
    #
    # The argument b is the block size used in the block splitting stage.
    # With JPEG encoding this is always 8.
    #
    # Copyright Joshua 2017

    img = cv2.imread(filename)

    # only sample the part of the image that is a multiple of 8 pixels wide
    # and multiple of 8 pixels high
    h, w = np.array(img.shape[:2]) // b * b
    img = img[:h, :w]

    # convert the image to RGB - the image is read by OpenCV in BGR format
    rgb_img = np.zeros(img.shape, np.uint8)
    rgb_img[:, :, 0] = img[:, :, 2]
    rgb_img[:, :, 1] = img[:, :, 1]
    rgb_img[:, :, 2] = img[:, :, 0]

    return rgb_img

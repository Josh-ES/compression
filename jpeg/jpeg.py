from binary import bits_to_bytes
from colourspace import transform, rgb_to_yuv
from dct import dct
from encode import vl_encode
from huffman import huffman
import numpy as np
import rle
from zigzag import zigzag_order
from .block_split import block_split
from .quantise import quantise


def jpeg(image):
    # jpeg(filename) applies the simplified JPEG encoding algorithm to the
    # image file given by the filename passed as an argument.
    #
    # Copyright Joshua 2017

    # get the shape of the image itself
    shape = np.shape(image)

    # transform to YUV
    yuv_image = transform(image, rgb_to_yuv)

    # split into blocks
    y_blocks, u_blocks, v_blocks = block_split(yuv_image)

    # set up an array for run length encoded bytes
    rle_bytes = []

    # loop through each luminance block now, and perform the JPEG process on it
    for block in y_blocks:
        # discrete cosine transform
        dct_block = dct(block)

        # then quantise
        qu_block = quantise(dct_block, False)

        # zig zag order and run length encoding
        ordered_elements = zigzag_order(qu_block)
        symbols, binary = rle.encode([int(el) for el in ordered_elements], numeric=True)

        # convert to literal bytes (as integers)
        binary += '0' * (8 - len(binary) % 8)
        rle_bytes += bits_to_bytes(binary) + [256]

    # loop through each chrominance block, and perform the JPEG process on it using
    # a different quantisation matrix this time
    for block in u_blocks + v_blocks:
        # discrete cosine transform
        dct_block = dct(block)

        # then quantise
        qu_block = quantise(dct_block, True)

        # zig zag order and run length encoding
        ordered_elements = zigzag_order(qu_block)
        symbols, binary = rle.encode([int(el) for el in ordered_elements], numeric=True)

        # convert to literal bytes (as integers)
        binary += '0' * (8 - len(binary) % 8)
        rle_bytes += bits_to_bytes(binary) + [256]

    # we need the probability distribution of the rle encoded data
    hist, bin_edges = np.histogram([data for data in rle_bytes], bins=range(0, 258))
    total = np.sum(hist)
    p_rle_encoded = [n / total for n in hist]

    # generate the codes and huffman encode the rle encoded bytes
    codes = huffman(p_rle_encoded)
    huff_encoded = vl_encode(rle_bytes, codes)

    return shape, codes, huff_encoded

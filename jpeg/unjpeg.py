from binary import bytes_to_bits
from colourspace import transform, yuv_to_rgb
from dct import inverse_dct
from encode import vl_decode
import rle
from zigzag import unzigzag_order
from .dequantise import dequantise
from .unblock_split import unblock_split


def unjpeg(shape, codes, bits):
    # unjpeg(shape, codes, bits) applies the simplified JPEG decoding algorithm to the
    # bits passed as an argument. It uses the passed codes as a codebook for huffman
    # decoding and uses the shape of the
    #
    # Copyright Joshua 2017

    # decode the document, undoing huffman coding
    huffman_decoded = vl_decode(bits, codes, bytes=True)

    # we then need to work out how many of each type of block we are expecting to construct
    # based on the shape of the desired image
    desired_blocks = int(shape[0] * shape[1] / 8 ** 2)

    # setup y, u and v block arrays
    y_blocks = []
    u_blocks = []
    v_blocks = []

    # loop through the decoded bytes
    n_blocks = 0
    buffer = []

    for byte in huffman_decoded:
        if byte == 256:
            # if the byte is 256, we've hit the end of the block so we should construct it
            #
            # to any block, we need to RLE decode the block's bytes, and unzigzag order the
            # elements into a square matrix
            decoded_elements = rle.decode(bytes_to_bits(buffer), numeric=True)
            square_matrix = unzigzag_order(decoded_elements)

            # we then need to dequantise, and inverse DCT each block to get the values back
            # into the YUV colour space, and add the blocks to 1-dimensional arrays of Y,
            # U and V blocks
            if n_blocks < desired_blocks:
                unquantised = dequantise(square_matrix, chrominance=False)
                inverse_dct_matrix = inverse_dct(unquantised)
                y_blocks.append(inverse_dct_matrix)
            elif n_blocks < 2 * desired_blocks:
                unquantised = dequantise(square_matrix, chrominance=True)
                inverse_dct_matrix = inverse_dct(unquantised)
                u_blocks.append(inverse_dct_matrix)
            else:
                unquantised = dequantise(square_matrix, chrominance=True)
                inverse_dct_matrix = inverse_dct(unquantised)
                v_blocks.append(inverse_dct_matrix)

            n_blocks += 1
            buffer = []
        else:
            buffer.append(byte)

    # we then need to arrange the resulting blocks in an image form
    image = unblock_split(shape, y_blocks, u_blocks, v_blocks)
    rgb_image = transform(image, yuv_to_rgb)

    return rgb_image

import numpy as np


def unblock_split(shape, blocks_1, blocks_2, blocks_3, block_size=8):
    # block_split(image) splits the image matrix into three arrays of blocks,
    # where each block is a block_size x block_size matrix.
    #
    # unblock_split(blocks_1, blocks_2, blocks_3) does the opposite of that,
    # reassembling an image 3D matrix from the input blocks

    image = np.empty((shape[0], shape[1], 3))
    image_blocks = []

    for index, block in enumerate(blocks_1):
        block_1 = np.array(block)
        block_2 = np.array(blocks_2[index])
        block_3 = np.array(blocks_3[index])

        # this stacks the blocks, rounds the values and returns them in int form
        image_blocks.append(np.dstack((block_1, block_2, block_3)).round())

    for i in range(0, shape[0], block_size):
        for j in range(0, shape[1], block_size):
            index = (i * shape[0] // block_size + j) // block_size
            image[i:i + 8, j:j + 8] = image_blocks[index]

    return image

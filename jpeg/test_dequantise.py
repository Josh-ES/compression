import unittest
import numpy as np
from .dequantise import dequantise


class TestJPEGDequantiser(unittest.TestCase):

    def testCorrectMatrixReturned(self):
        input_matrix = np.matrix([
            [-26, -3, -6, 2, 2, -1, 0, 0],
            [0, -2, -4, 1, 1, 0, 0, 0],
            [-3, 1, 5, -1, -1, 0, 0, 0],
            [-3, 1, 2, -1, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
        ])

        # dequantise the above matrix using the luminance quantisation table
        quantised_matrix = dequantise(input_matrix, False)

        expected_matrix = np.matrix([
            [-416, -33, -60, 32, 48, -40, 0, 0],
            [0, -24, -56, 19, 26, 0, 0, 0],
            [-42, 13, 80, -24, -40, 0, 0, 0],
            [-42, 17, 44, -29, 0, 0, 0, 0],
            [18, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
        ])

        # make a number of assertions about individual elements in the matrix
        np.testing.assert_array_equal(quantised_matrix, expected_matrix)


if __name__ == '__main__':
    unittest.main()

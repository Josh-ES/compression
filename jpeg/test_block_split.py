import unittest
import random
from .block_split import block_split
from .load_image import load_image


class TestBlockSplitter(unittest.TestCase):

    def testCorrectRGBValuesInBlocks(self):
        # set the block size explicitly for this test
        block_size = 8

        # run the image loader on an 8x8 image, with a pink colour with RGB:
        # rgb(236, 30, 98)
        img = load_image('jpeg/pink.png')

        # split the image into blocks
        r_blocks, g_blocks, b_blocks = block_split(img, block_size)

        # generate a random coordinate pair
        x = random.randint(0, 7)
        y = random.randint(0, 7)

        # test the red values
        r_block = r_blocks[0]
        self.assertEqual(r_block[y, x], 236)

        # test the green values
        g_block = g_blocks[0]
        self.assertEqual(g_block[y, x], 30)

        # test the blue values
        b_block = b_blocks[0]
        self.assertEqual(b_block[y, x], 98)

    def testBlocksOfCorrectSize(self):
        # set the block size explicitly for this test
        block_size = 8

        # run the image loader on an 8x8 image, with a pink colour with RGB:
        # rgb(236, 30, 98)
        img = load_image('jpeg/pink.png')

        # split the image into blocks
        r_blocks, g_blocks, b_blocks = block_split(img, block_size)

        for block in r_blocks + g_blocks + b_blocks:
            self.assertEqual(block.shape[0], block_size)
            self.assertEqual(block.shape[1], block_size)

    def testCorrectNumberOfBlocks(self):
        # set the block size explicitly for this test
        block_size = 8

        # run the image loader on a 512x512 image
        img = load_image('images/lena.pbm')

        # split the image into blocks
        r_blocks, g_blocks, b_blocks = block_split(img, block_size)

        # check the numbers of blocks are equal in each colour dimension
        self.assertEqual(len(r_blocks), len(g_blocks))
        self.assertEqual(len(g_blocks), len(b_blocks))

        # check that there are the right number of blocks
        self.assertEqual(len(r_blocks), 512 ** 2 / block_size ** 2)


if __name__ == '__main__':
    unittest.main()

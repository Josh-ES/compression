import numpy as np


def dequantise(input_matrix, chrominance=False):
    # dequantise(input_matrix, chrominance=False) reverses the JPEG quantisation
    # step
    #
    # Copyright Joshua

    # define two quantisation tables from the standard, one for chrominance
    # and one for luminance
    if chrominance:
        quantisation_table = np.matrix([
            [17, 18, 24, 47, 99, 99, 99, 99],
            [18, 21, 26, 66, 99, 99, 99, 99],
            [24, 26, 56, 99, 99, 99, 99, 99],
            [47, 66, 99, 99, 99, 99, 99, 99],
            [99, 99, 99, 99, 99, 99, 99, 99],
            [99, 99, 99, 99, 99, 99, 99, 99],
            [99, 99, 99, 99, 99, 99, 99, 99],
            [99, 99, 99, 99, 99, 99, 99, 99],
        ])
    else:
        quantisation_table = np.matrix([
            [16, 11, 10, 16, 24, 40, 51, 61],
            [12, 12, 14, 19, 26, 58, 60, 55],
            [14, 13, 16, 24, 40, 57, 69, 56],
            [14, 17, 22, 29, 51, 87, 80, 62],
            [18, 22, 37, 56, 68, 109, 103, 77],
            [24, 35, 55, 64, 81, 104, 113, 92],
            [49, 64, 78, 87, 103, 121, 120, 101],
            [72, 92, 95, 98, 112, 100, 103, 99],
        ])

    shape = np.shape(input_matrix)

    # check that the shape of the input matrix is 8x8
    if shape != (8, 8):
        raise ValueError('The input matrix is not an 8x8 matrix')

    # create the output matrix
    output_matrix = np.zeros(shape)

    # and then, build up the result using the input matrix and quantisation
    # table
    for k in range(0, shape[1]):
        for j in range(0, shape[0]):
            output_matrix[k, j] = input_matrix[k, j] * quantisation_table[k, j]

    # return the result
    return output_matrix

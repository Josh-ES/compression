import cv2
import numpy as np


def save_image(filename, image):
    # save_image(image) saves the image passed as a 3D matrix data
    # structure at a location specified by filename.
    #
    # The method uses OpenCV, as this is the easiest Python method to
    # read images' RGB data
    #
    # Copyright Joshua 2017

    # ensure input image is in NumPy array format
    image = np.array(image)

    # convert the image to BGR - the image is saved by OpenCV in BGR format,
    # but input in RGB format
    bgr_image = np.zeros(image.shape, np.uint8)
    bgr_image[:, :, 0] = image[:, :, 2]
    bgr_image[:, :, 1] = image[:, :, 1]
    bgr_image[:, :, 2] = image[:, :, 0]

    cv2.imwrite(filename, bgr_image)

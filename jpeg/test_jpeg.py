import unittest
from .jpeg import jpeg
from .load_image import load_image


class TestJPEGEncoder(unittest.TestCase):

    def testOutputsCorrectJPEGString(self):
        # load the test image
        image = load_image('jpeg/pink.png')

        # run the jpeg encoder on an 8x8 image, with a pink colour with RGB:
        # rgb(236, 30, 98)
        shape, codes, bits = jpeg(image)

        # # write codes to a file
        # file = open('jpeg/test.codebook', 'w')
        #
        # for cwd in codes:
        #     file.write("%s\n" % cwd)
        #
        # file.close()

        self.assertEqual(shape[0], 8)
        self.assertEqual(shape[1], 8)
        self.assertEqual(len(codes), 257)


if __name__ == '__main__':
    unittest.main()

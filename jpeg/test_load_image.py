import unittest
import numpy as np
import random
from .load_image import load_image


class TestImageLoader(unittest.TestCase):

    def testReadsCorrectRGBValues(self):
        # run the image loader on an 8x8 image, with a pink colour with RGB:
        # rgb(236, 30, 98)
        img = load_image('jpeg/pink.png')

        # pick a random pixel in the image
        x = random.randint(0, 7)
        y = random.randint(0, 7)

        # assert that the pixel is the correct colour
        np.testing.assert_array_equal(img[x, y], [236, 30, 98])


if __name__ == '__main__':
    unittest.main()

import unittest
import random
import numpy as np
from .unjpeg import unjpeg


class TestJPEGDecoder(unittest.TestCase):

    def testOutputsCorrectImage(self):
        file = open('jpeg/test.codebook', 'r')
        codes = [line.rstrip() for line in file.readlines()]
        file.close()

        # set the shape of the desired image
        shape = (8, 8)

        # set the bit pattern that we output
        bits = '110001000001100111010101110100000110'

        rgb_image = unjpeg(shape, codes, bits)

        # generate random coordinates and assert the colour of the image is correct
        x = random.randint(0, 7)
        y = random.randint(0, 7)
        np.testing.assert_array_equal(rgb_image[y, x], [237, 30, 100])


if __name__ == '__main__':
    unittest.main()

import unittest
from .bits_to_coded_bytes import bits_to_coded_bytes


class TestBitsToCodedBytes(unittest.TestCase):

    def testCorrectBytesReturned(self):
        bytes = bits_to_coded_bytes('10000')
        self.assertEqual(bytes, [16])

    def testQuizBytesReturned(self):
        bytes = bits_to_coded_bytes('011011000')
        self.assertEqual(bytes, [141, 128])

if __name__ == '__main__':
    unittest.main()

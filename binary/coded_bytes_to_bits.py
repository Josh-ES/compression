def coded_bytes_to_bits(input_bytes):
    # bytes2bits(x) converts a list of bytes (values between 0 and 255) to a
    #  string of bits.
    #
    # make note of the format in which the incoming bytes are formatted
    #
    # e.g. take binary symbol 10000, this is represented as:
    #
    #    0 0 0
    #    x x x 1 0 0 0 0    = 16 (as a byte, in decimal form)
    #    _____ _________
    #
    #    ^             ^
    #    number of     the original binary vector,
    #    bits to       no bits need be added here
    #    add to
    #    ensure
    #    bytes
    #
    # Copyright Joshua 2017

    bits = ''

    for byte in input_bytes:
        # get the binary string from the numeric byte and sum the bits together
        binary = '{:08b}'.format(byte)
        bits += binary

    # find the number of bits added and the actual binary value itself
    bits_to_add = int(bits[0:3], 2)
    binary_value = bits[3:]

    # return the binary with the added characters removed from the working binary string
    return binary_value[0:(-bits_to_add if bits_to_add > 0 else None)]

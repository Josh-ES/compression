def str_to_bits(text):
    binary = ''

    for c in text:
        ascii_code = ord(c)
        binary += '{0:08b}'.format(ascii_code)

    return binary

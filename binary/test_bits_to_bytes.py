import unittest
from .bits_to_bytes import bits_to_bytes


class TestBitsToLiteralBytes(unittest.TestCase):

    def testCorrectBytesReturned(self):
        literal_bytes = bits_to_bytes('001000001001000010001000010100000000001101101000000000111001010000000'
                                      '010110010100000000001000110')

        self.assertEqual(literal_bytes, [32, 144, 136, 80, 3, 104, 3, 148, 2, 202, 0, 70])


if __name__ == '__main__':
    unittest.main()

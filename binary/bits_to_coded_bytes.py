def bits_to_coded_bytes(bits):
    # Converts a binary string `bits` to a list of bytes
    #  (group of 8 bits). Not as trivial as it sounds, because the number
    #  of bits may not be divisible by 8. If we add zeros at the end to make
    #  it divisible by 8 we may be adding codewords to the end of a
    #  compressed stream.
    #
    # Between 0 and 7 bits must be added to the end of an input vector to
    #  make it divisible by 8, which can be stored as a 3-bit prefix.
    #
    # e.g. take binary symbol 10000
    #
    #    0 0 0
    #    x x x 1 0 0 0 0    = 16 (as a byte, in decimal form)
    #    _____ _________
    #
    #    ^             ^
    #    number of     the original binary vector,
    #    bits to       no bits need be added here
    #    add to
    #    ensure
    #    bytes

    n = len(bits) + 3
    bits_to_add = (8 - n % 8) % 8
    binary = '{0:03b}'.format(bits_to_add) + bits + bits_to_add * '0'
    byte_chunks = [binary[i:i + 8] for i in range(0, len(binary), 8)]
    return [int(byte, 2) for byte in byte_chunks]

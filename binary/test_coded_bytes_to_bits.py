import unittest
from .coded_bytes_to_bits import coded_bytes_to_bits
from .bits_to_coded_bytes import bits_to_coded_bytes


class TestCodedBytesToBits(unittest.TestCase):

    def testCorrectBinaryProduced(self):
        bits = coded_bytes_to_bits([16])
        self.assertEqual(bits, '10000')

    def testRealFileBits(self):
        all_bits = '0000000000000000010110101010100101101010010010100110100110100110000000'
        coded_bytes = bits_to_coded_bytes(all_bits)
        bits = coded_bytes_to_bits(coded_bytes)
        self.assertEqual(all_bits, bits)


if __name__ == '__main__':
    unittest.main()

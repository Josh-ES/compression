import unittest
from .str_to_bits import str_to_bits
from .bits_to_str import bits_to_str


class TestStringToBits(unittest.TestCase):

    def testAgainstIndividualCharacter(self):
        bits = str_to_bits('A')
        self.assertEqual(bits, '01000001')

    def testAgainstFailingCaseFromDEFLATEImplementation(self):
        bits = str_to_bits(bits_to_str('001000001001000010001000010100000000001101101000000000111001010000000'
                                       '010110010100000000001000110'))

        self.assertEqual(bits, '00100000100100001000100001010000000000110110100000000011100101000000001011001'
                               '0100000000001000110')


if __name__ == '__main__':
    unittest.main()

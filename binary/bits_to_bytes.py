def bits_to_bytes(bits):
    # Converts a binary string `bits` to a list of bytes
    #  (group of 8 bits). This contrasts with the 'coded bytes' routine,
    #  which doesn't simply add trailing zeros due to the issue this
    #  could cause with codewords. This method just converts bits to
    #  byte integers literally.

    return [int(bits[i:i + 8], 2) for i in range(0, len(bits), 8)]

import unittest
from .bits_to_str import bits_to_str


class TestBitsToString(unittest.TestCase):

    def testAgainstIndividualCharacter(self):
        plaintext = bits_to_str('01000001')
        self.assertEqual(plaintext, 'A')


if __name__ == '__main__':
    unittest.main()

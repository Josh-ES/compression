def bits_to_str(bits):
    string = ''

    for i in range(len(bits) // 8):
        binary = bits[i * 8:i * 8 + 8]
        ascii_code = int(binary, 2)
        string += chr(ascii_code)

    return string

def bytes_to_bits(input_bytes):
    # Converts a list of bytes (group of 8 bits) to a binary string `bits`.
    #  This method just converts byte integers to bits literally, and does
    #  not include capability for handling extra coded zeros added in the
    #  'coded bytes' methods.

    return ''.join(['{0:08b}'.format(i) for i in input_bytes])

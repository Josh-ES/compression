import unittest
from .bytes_to_bits import bytes_to_bits


class TestLiteralBytesToBits(unittest.TestCase):

    def testCorrectBytesReturned(self):
        bits = bytes_to_bits([32, 144, 136, 80, 3, 104, 3, 148, 2, 202, 0, 70])

        self.assertEqual(bits, '001000001001000010001000010100000000001101101000000000111001010000000'
                               '010110010100000000001000110')


if __name__ == '__main__':
    unittest.main()

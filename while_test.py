from math import ceil

# 6 bit integer values
precision = 6

# this is the max_integer
one = (2 ** precision) - 1
quarter = ceil(one / 4)
half = 2 * quarter
three_quarters = 3 * quarter

input = '0001110'

lo = 3
hi = 35
value = int(input[0:precision], 2)

xp = precision
# lo = 22; hi = 50

while True:
    if hi < half:
        # do nothing
        pass
    elif lo >= half:
        lo -= half
        hi -= half
        value -= half
    elif (lo >= quarter) and (hi < three_quarters):
        # interval within [1/4, 3/4)
        lo -= quarter
        hi -= quarter
        value -= quarter
    else:
        break

    lo *= 2
    hi = hi * 2 + 1
    value = value * 2 + int(input[xp], 2)
    xp += 1

    if xp == len(input) + 1:
        raise Exception('Unable to decompress')
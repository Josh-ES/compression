#!/usr/bin/env python3

import numpy as np
from arithmetic import arithmetic
from binary import bits_to_coded_bytes
from stats import entropy
from sys import argv


def camzip3(filename):
    file = open(filename, 'rb')
    file_input = file.read()
    file.close()

    # the sequence of integers used as bins for the histogram define the edges of the bins, so we need to
    # include 256. values are placed into bins according to a <= value < b, where a and b are the edges
    # of the relevant bin
    hist, bin_edges = np.histogram([data for data in file_input], bins=range(0, 256 + 1))
    total = np.sum(hist)
    p = [n / total for n in hist]
    file_length = len(file_input)

    out = arithmetic(file_input, p)
    hamlet_cz3 = bits_to_coded_bytes(out)

    print('Actual Compression Performance:', len(hamlet_cz3) / len(file_input) * 8, 'bits per byte')
    print('Theoretical Compression Performance:', entropy(p), 'bits per byte')

    # write the binary code to a file
    file = open(filename + '.cz3', 'wb')
    file.write(bytearray(hamlet_cz3))
    file.close()

    # write the code table to a file
    file = open(filename + '.cz3c', 'w')

    file.write('%s\n' % file_length)

    for prob in p:
        # note we need to ensure the precision is high enough to decode the files, 22 dp is probably enough
        file.write('%.22f\n' % prob)

    file.close()

# read the filename from the arguments to the function
filename = argv[1]
camzip3(filename)

import unittest
from .deflate import deflate


class TestDEFLATEImplementation(unittest.TestCase):

    def testCorrectEncodedBitsReturned(self):
        ciphertext, codes = deflate('ABBABBABBBAABABA')
        self.assertEqual(ciphertext, '1000001000000100100011010011011101110110101')


if __name__ == '__main__':
    unittest.main()

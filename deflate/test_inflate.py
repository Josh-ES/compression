import unittest
from .inflate import inflate


class TestDEFLATEImplementation(unittest.TestCase):

    def testCorrectCodesReturned(self):
        file = open('deflate/test_codebook.defc', 'r')
        codes = [line.rstrip() for line in file.readlines()]
        file.close()

        plaintext = inflate('1000001000000100100011010011011101110110101', codes)
        self.assertEqual(plaintext, 'ABBABBABBBAABABA')


if __name__ == '__main__':
    unittest.main()

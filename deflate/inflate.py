from binary import str_to_bits
from encode import vl_decode
from lz77 import lz77_decode


def inflate(x, codes):
    # inflate(x, codes) implements a decoder of the simple DEFLATE algorithm implementation used
    # in this project. First, a Huffman decoder decompresses the original sequence, using the
    # probability distribution p to generate the codes. Then, LZ77 decompression is applied to
    # recover the original source sequence. The Huffman codebook is passed as the argument codes.
    #
    # Actual implementation uses an LZSS decoder, which accounts better for cases where there is
    # no match and an uncompressed character must be sent, and only uses a match if it
    # is longer than 2 characters (bytes).
    #
    # Reads in a binary string of '0' and '1' characters, and outputs plaintext, to align with
    # the rest of the source code in this project.
    #
    # Copyright Joshua 2017

    # decode the document, undoing huffman coding
    huffman_decoded = vl_decode(x, codes)

    # we then need to lz77 decode the resulting string and return the plaintext result
    decoded_bits = str_to_bits(huffman_decoded)
    return lz77_decode(decoded_bits)

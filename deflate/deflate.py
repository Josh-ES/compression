from binary import bits_to_bytes
from encode import vl_encode
from huffman import huffman
from lz77 import lz77_encode
from stats import entropy

import numpy as np


def deflate(x):
    # deflate(x) implements a simplification of the DEFLATE algorithm for a source
    # sequence x. LZ77 compresses by replacing repeated occurrences of data with
    # references to a copy existing earlier. Then, Huffman coding compresses the resulting
    # sequence.
    #
    # Actual implementation uses LZSS, which accounts better for cases where there is
    # no match and an uncompressed character must be sent, and only uses a match if it
    # is longer than 2 characters (bytes).
    #
    # Huffman coding requires an intermediate probability distribution to be generated
    # midway through the algorithm, in this case. Adaptive huffman coding could be used
    # for a stream of data.
    #
    # Copyright Joshua 2017

    # first encode using lz77
    lz77_encoded = lz77_encode(x)

    # convert to literal bytes (as integers)
    lz77_encoded += '0' * (8 - len(lz77_encoded) % 8)
    first_pass = bits_to_bytes(lz77_encoded)

    # print LZ77 performance, note that lz77_encoded is in bits and x is in bytes as it is
    # an ASCII string
    # print('LZ77 Compression Performance:', len(lz77_encoded) / len(x), 'bits per byte')

    # we need the probability distribution of the lz77 encoded data
    hist, bin_edges = np.histogram([data for data in first_pass], bins=range(0, 257))
    total = np.sum(hist)
    p_lz77_encoded = [n / total for n in hist]

    # then encode using huffman coding, generating codes for the lz77 encoded string and encoding
    codes = huffman(p_lz77_encoded)

    # write the code table to a file
    # file = open('deflate/test_codebook.defc', 'w')
    #
    # for cwd in codes:
    #     file.write("%s\n" % cwd)
    #
    # file.close()

    huff_encoded = vl_encode(first_pass, codes)

    # print huffman performance, note that huff_encoded is in bits and x is in bytes as it is
    # an ASCII string
    # print('Huffman Compression Performance:', len(huff_encoded) / len(lz77_encoded) * 8, 'bits per byte')
    # print('Theoretical Huffman Compression Performance:', entropy(p_lz77_encoded), 'bits per byte')

    # return both the encoded bits, and the codes which we will need to record
    return huff_encoded, codes

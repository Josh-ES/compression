from binarytree import Node
from tree import tree


def tree_to_code(tr, cwd='', codewords=None):
    # tree2code(t) converts a tree data structure into a list of
    # codewords as strings

    if codewords is None:
        codewords = []

    if not isinstance(tr, Node):
        tr = tree(tr)

    if tr.left:
        codewords = tree_to_code(tr.left, cwd + '0', codewords)

    if tr.right:
        codewords = tree_to_code(tr.right, cwd + '1', codewords)

    if tr.left is None and tr.right is None:
        codewords.append(cwd)

    return codewords

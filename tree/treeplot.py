from binarytree import show

from .tree import tree


def treeplot(nodes):
    # When a list is passed to the treeplot method, e.g. treeplot([0, 1, 2, 2, 1])
    #  a binary tree is constructed and displayed on the screen.

    tr = tree(nodes)
    show(tr)

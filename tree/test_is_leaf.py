import unittest
from tree import is_leaf
from binarytree import Node


class TestWhetherSomethingIsALeafOrNot(unittest.TestCase):

    def testIsNotALeaf(self):
        root = Node(1)
        root.left = Node('A')
        root.right = Node(2)
        is_it_a_leaf = is_leaf(root)
        self.assertEqual(is_it_a_leaf, False)

    def testIsALeaf(self):
        root = Node(1)
        is_it_a_leaf = is_leaf(root)
        self.assertEqual(is_it_a_leaf, True)

if __name__ == '__main__':
    unittest.main()
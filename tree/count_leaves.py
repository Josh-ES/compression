def count_leaves(tree, count=0):
    if tree.left is None and tree.right is None:
        count += 1

    if tree.left is not None:
        count = count_leaves(tree.left, count)

    if tree.right is not None:
        count = count_leaves(tree.right, count)

    return count

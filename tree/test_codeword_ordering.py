import unittest
from tree import code_to_tree, tree_to_code


class TestCodewordOrdering(unittest.TestCase):

    def testCodewordsEndUpInSameOrder(self):
        correct_codes = tree_to_code(code_to_tree(['0', '1']))
        reordered_codes = tree_to_code(code_to_tree(['1', '0']))
        self.assertEqual(len(correct_codes), len(reordered_codes))
        self.assertEqual(correct_codes[0], reordered_codes[0])
        self.assertEqual(correct_codes[1], reordered_codes[1])

if __name__ == '__main__':
    unittest.main()

import unittest
from tree import code_to_tree


class TestCodeToTreeMethod(unittest.TestCase):

    def testCorrectTreeConstructed(self):
        tr = code_to_tree(['00', '01', '1'])
        self.assertIsNotNone(tr.left)
        self.assertIsNotNone(tr.right)
        self.assertIsNotNone(tr.left.left)
        self.assertIsNotNone(tr.left.right)

    def testCorrectLeavesOnTree(self):
        tr = code_to_tree(['00', '01', '1'])
        self.assertIsNone(tr.right.left)
        self.assertIsNone(tr.right.right)
        self.assertIsNone(tr.left.left.left)
        self.assertIsNone(tr.left.left.right)
        self.assertIsNone(tr.left.right.left)
        self.assertIsNone(tr.left.right.right)

if __name__ == '__main__':
    unittest.main()

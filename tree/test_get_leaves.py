import unittest
from tree import get_leaves
from binarytree import Node


class TestLeafCounterWorking(unittest.TestCase):

    def testCorrectNumberOfLeavesReturned(self):
        root = Node(1)
        root.left = Node('A')
        root.right = Node(2)
        leaves = get_leaves(root)
        self.assertEqual([leaf.value for leaf in leaves], ['A', 2])

    def testCaseWithMultipleLeaves(self):
        root = Node(1)
        root.left = Node('A')
        root.right = Node(2)
        root.right.left = Node(3)
        root.right.right = Node(4)
        root.right.right.left = Node('B')
        leaves = get_leaves(root)
        self.assertEqual([leaf.value for leaf in leaves], ['A', 3, 'B'])

if __name__ == '__main__':
    unittest.main()
import unittest
from tree import count_leaves
from binarytree import Node, show


class TestLeafCounterWorking(unittest.TestCase):

    def testCorrectNumberOfLeavesReturned(self):
        root = Node(1)
        root.left = Node('A')
        root.right = Node(2)
        count = count_leaves(root)
        self.assertEqual(count, 2)

    def testCaseWithMultipleLeaves(self):
        root = Node(1)
        root.left = Node('A')
        root.right = Node(2)
        root.right.left = Node(3)
        root.right.right = Node(4)
        root.right.right.left = Node('B')
        count = count_leaves(root)
        self.assertEqual(count, 3)

if __name__ == '__main__':
    unittest.main()
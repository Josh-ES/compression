from tree import tree


def tree_to_newick(tr, labels=[]):
    # When a list is passed to the treeplot method, e.g. treeToNewick([0, 1, 2, 2, 1])
    #  a binary tree is constructed and the tree in 'Newick format' is returned.
    
    # binarytree2newick(t) takes a tree t in MATLAB's pointer format (list of
    # pointers where each node points to its parent, pointer zero is the root
    # node) to Newick format (see https://en.wikipedia.org/wiki/Newick_format)
    # for use for example in tree representation software such as 
    # http://phylo.io
    #
    # By default the nodes are simply numbered 1:length(t). The user can supply
    # node labels as an optional argument 'labels'.
    #
    # The extra parameter n is used internally when calling itself recursively
    # and should not be supplied by the user.
    #
    # Copyright Jossy, 2016
    # Translation Josh, 2017

    tr = tree(tr)

    value = tr.value
    label = labels[value - 1] if len(labels) >= value else str(value)

    children = [tr.left, tr.right]
    descendants = ','.join([tree_to_newick(n, labels) for n in children if n is not None])

    if descendants:
        descendants = '(' + descendants + ')'

    return descendants + label

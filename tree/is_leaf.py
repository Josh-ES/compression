def is_leaf(tree):
    if tree.left is None and tree.right is None:
        return True

    return False

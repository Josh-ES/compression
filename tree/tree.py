from binarytree import Node


def tree(nodes):
    # When a list is passed to the tree method, e.g. tree([0, 1, 2, 2, 1])
    #  a binary tree is constructed. Each entry in the list is one node in
    #  the tree, and each value is the index of its parent. The root node is
    #  of index 1.
    #
    # e.g. tree([0, 1, 2, 2, 1]) produces:
    #
    #     __1
    #    /   \
    #   2     5
    #  / \
    # 3   4

    if isinstance(nodes, Node):
        return nodes

    # only return anything if the length of the passed list is greater than 0
    if len(nodes) > 0:
        # create the root node, and a list to refer back to
        root = Node(1)
        tree_nodes = [root]

        # create a node for each value in the passed list
        for index, n in enumerate(nodes[1:]):
            parent = tree_nodes[n - 1]
            node = Node(index + 2)

            # start building the tree from the left-hand side
            if not parent.left:
                parent.left = node
            else:
                parent.right = node

            tree_nodes.append(node)

        # return a pointer to the root node of the tree
        return root

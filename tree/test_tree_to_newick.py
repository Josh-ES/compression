import unittest
from tree import tree_to_newick


class TestTreeToNewickMethod(unittest.TestCase):

    def testCorrectNewickForm(self):
        newick_str = tree_to_newick([0, 1, 2, 2, 1])
        self.assertEqual(newick_str, '((3,4)2,5)1')

if __name__ == '__main__':
    unittest.main()

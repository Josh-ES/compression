import unittest
from tree import tree_to_code


class TestTreeToCodeMethod(unittest.TestCase):

    def testCorrectNumberOfCodes(self):
        codes = tree_to_code([0, 1, 2, 2, 1])
        self.assertEqual(len(codes), 3)

    def testCorrectCodesGenerated(self):
        codes = tree_to_code([0, 1, 2, 2, 1])
        self.assertEqual(codes[0], '00')
        self.assertEqual(codes[1], '01')
        self.assertEqual(codes[2], '1')

if __name__ == '__main__':
    unittest.main()

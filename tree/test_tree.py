import unittest
from binarytree import Node
from tree import tree


class TestTreeMethod(unittest.TestCase):

    def testEmptyTreeIsNone(self):
        self.assertIsNone(tree([]))

    def testTreeHasRoot(self):
        tr = tree([0])
        self.assertIsInstance(tr, Node)

    def testTreeHasChildNodes(self):
        tr = tree([0, 1, 1])
        self.assertIsInstance(tr.left, Node)
        self.assertIsInstance(tr.right, Node)

    def testTreeBuildsFromLeft(self):
        tr = tree([0, 1])
        self.assertIsInstance(tr.left, Node)
        self.assertIsNone(tr.right)

if __name__ == '__main__':
    unittest.main()

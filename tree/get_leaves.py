def get_leaves(tree, leaves=None):
    if leaves is None:
        leaves = []

    if tree.left is None and tree.right is None:
        leaves.append(tree)

    if tree.left is not None:
        leaves = get_leaves(tree.left, leaves)

    if tree.right is not None:
        leaves = get_leaves(tree.right, leaves)

    return leaves

from binarytree import Node
from xtree import xtree


def xtree_to_code(tr, cwd='', codewords=None, values=None, order_by_leaf_values=False):
    # only return something if tr is not None
    if tr is not None:
        # tree2code(t) converts a tree data structure into a list of
        # codewords as strings
        if codewords is None:
            codewords = []

        if values is None:
            values = []

        if not isinstance(tr, Node):
            tr = xtree(tr)

        left_value = tr.left.value if tr.left is not None else None
        right_value = tr.right.value if tr.right is not None else None

        if left_value is None \
                or right_value is None \
                or not type(left_value) is type(right_value) \
                or left_value < right_value:
            codewords = xtree_to_code(tr.left, cwd + '0', codewords, values, order_by_leaf_values)
            codewords = xtree_to_code(tr.right, cwd + '1', codewords, values, order_by_leaf_values)
        else:
            codewords = xtree_to_code(tr.right, cwd + '1', codewords, values, order_by_leaf_values)
            codewords = xtree_to_code(tr.left, cwd + '0', codewords, values, order_by_leaf_values)

        if tr.left is None and tr.right is None:
            codewords.append(cwd)
            values.append(tr.value)

    if order_by_leaf_values:
        sorted_cwds = sorted(enumerate(codewords), key=lambda x: values[x[0]])
        values.sort()
        codewords = [cwd[1] for cwd in sorted_cwds]

    return codewords

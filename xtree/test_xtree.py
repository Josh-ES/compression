import unittest
from .xtree import xtree


class TestTreeMethod(unittest.TestCase):

    def testEmptyTreeIsNone(self):
        self.assertIsNone(xtree([]))

    def testTreeConstruction(self):
        tr = xtree([
            [0, 1, 2, 2, 1],
            [2, 3, 0, 0, 0],
            [5, 4, 0, 0, 0]
        ])

        self.assertIsNotNone(tr.left)
        self.assertIsNotNone(tr.right)
        self.assertIsNotNone(tr.left.left)
        self.assertIsNotNone(tr.left.right)

    def testLeafConstruction(self):
        tr = xtree([
            [0, 1, 2, 2, 1],
            [2, 3, 0, 0, 0],
            [5, 4, 0, 0, 0]
        ])

        self.assertIsNone(tr.right.left)
        self.assertIsNone(tr.right.right)
        self.assertIsNone(tr.left.left.left)
        self.assertIsNone(tr.left.left.right)
        self.assertIsNone(tr.left.right.left)
        self.assertIsNone(tr.left.right.right)

if __name__ == '__main__':
    unittest.main()

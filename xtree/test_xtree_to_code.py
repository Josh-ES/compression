import unittest
from .code_to_xtree import code_to_xtree
from .xtree_to_code import xtree_to_code


class TestXtreeToCode(unittest.TestCase):

    def testCodes(self):
        tr = code_to_xtree(['0', '1'])
        code = xtree_to_code(tr)
        self.assertEqual(len(code), 2)
        self.assertEqual(code[0], '0')
        self.assertEqual(code[1], '1')

    def testCodesInDifferentOrder(self):
        tr = code_to_xtree(['1', '0'])
        code = xtree_to_code(tr)
        self.assertEqual(len(code), 2)
        self.assertEqual(code[0], '1')
        self.assertEqual(code[1], '0')

if __name__ == '__main__':
    unittest.main()

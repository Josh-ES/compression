from binarytree import Node


def xtree(xtree_form):
    # When a list is passed to the tree method, e.g. tree([0, 1, 2, 2, 1])
    #  a binary tree is constructed. Each entry in the list is one node in
    #  the tree, and each value is the index of its parent. The root node is
    #  of index 1.
    #
    # e.g. tree([0, 1, 2, 2, 1]) produces:
    #
    #     __1
    #    /   \
    #   2     5
    #  / \
    # 3   4

    if isinstance(xtree_form, Node):
        return xtree_form

    # the xtree form should consist of three separate lists of the same length
    if len(xtree_form) == 3:
        # split out the arrays
        nodes = xtree_form[0]
        left_children = xtree_form[1]
        right_children = xtree_form[2]

        # only return anything if the length of the passed list is greater than 0
        if len(nodes) > 0:
            # create a list to refer back to
            tree_nodes = {}

            # create a node for each value in the passed list
            for index, n in enumerate(nodes):
                # create the node at the given index if it does not already exist
                if not index + 1 in tree_nodes:
                    tree_nodes[index + 1] = Node(index + 1)

                # build out the node's children
                node = tree_nodes[index + 1]

                if not left_children[index] == 0:
                    if left_children[index] in tree_nodes:
                        node.left = tree_nodes[left_children[index]]
                    else:
                        node.left = Node(left_children[index])
                        tree_nodes[left_children[index]] = node.left

                if not right_children[index] == 0:
                    if right_children[index] in tree_nodes:
                        node.right = tree_nodes[right_children[index]]
                    else:
                        node.right = Node(right_children[index])
                        tree_nodes[right_children[index]] = node.right

            # return a pointer to the root node of the tree
            return tree_nodes[1]

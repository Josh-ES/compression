from binarytree import Node


def code_to_xtree(codes):
    # code_to_xtree(codes) converts a list of codewords (as strings)
    #  into an extended tree representation of the prefix-free code
    #  where every node points to its parent and child(ren) node(s).

    # only return anything if the length of the passed list of codes is greater than 0
    if len(codes) > 0:
        # create the root node
        root = Node(1)
        i = 2

        # loop through each codeword creating a tree branch from each codeword
        for index, codeword in enumerate(codes):
            # store the currently active node during construction
            active_node = root

            for transition in codeword:
                if transition == '0':
                    # create a new left node if one doesn't already exist
                    if active_node.left is None:
                        active_node.left = Node(i)
                        i += 1

                    active_node = active_node.left
                else:
                    # create a new right node if one doesn't already exist
                    if active_node.right is None:
                        active_node.right = Node(i)
                        i += 1

                    active_node = active_node.right

        return root

import unittest
from .code_to_xtree import code_to_xtree


class TestCodeToXtree(unittest.TestCase):

    def testTreeValues(self):
        tr = code_to_xtree(['0', '1'])
        self.assertEqual(tr.left.value, 2)
        self.assertEqual(tr.right.value, 3)

    def testTreeValuesInDifferentOrder(self):
        tr = code_to_xtree(['1', '0'])
        self.assertEqual(tr.left.value, 3)
        self.assertEqual(tr.right.value, 2)

if __name__ == '__main__':
    unittest.main()

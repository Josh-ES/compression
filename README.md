Josh Efiong: Compression Repository
===================================

[![Build Status](https://circleci.com/bb/Josh-ES/compression.png?circle-token=5ee36cc358b346c95551f7b6434d9a4f8ab8e15f)](https://circleci.com/bb/Josh-ES/compression)

Requirements
------------

You need Python 3.4 or later to run the scripts in this repository.  You can have multiple Python versions (2.x and 3.x) installed on the same system without problems.

In Ubuntu, Mint and Debian you can install Python 3 like this:

```
    $ sudo apt-get install python3 python3-pip
```

For other Linux flavors, OS X and Windows, packages are available at

  http://www.python.org/getit/

Python is usually pre-installed on OS X.

Dependencies
------------

This project has a number of dependencies.

* binarytree: used by the Huffman coding and Shannon-Fano coding implementations to construct binary trees when generating the codebooks.
* bitstring: used to convert integers into binary strings in the run-length encoding implementation used by the JPEG encoder.
* matplotlib: used in testing the JPEG implementation.
* numpy: used to represent matrices internally, as well as to make testing assertions against arrays or array values.
* opencv-python: used to read and save image data to portable bitmap (.pbm) files.

These dependencies are all saved in a `requirements.txt` file, and they can be installed by running:

```
    $ pip install -r requirements.txt
```

Wrapper Scripts
---------------

There are various wrapper scripts that perform various functions. Each wrapper script can be run like the following:

```
    $ ./camzip2.py <filename>
```

To compress a particular file specified in the `<filename>` argument. The list of wrapper scripts and the algorithms they use for data compression is given below:

* camzip1.py and camunzip1.py: applies Shannon-Fano coding and decoding to the file.
* camzip2.py and camunzip2.py: applies Huffman coding.
* camzip3.py and camunzip3.py: applies arithmetic coding.
* camzip4.py and camunzip4.py: applies adaptive arithmetic coding.
* camlz.py and camunlz.py: applies the LZ77 algorithm to the file.
* camdeflate.py and caminflate.py: applies the DEFLATE algorithm.
* camjpeg.py and camunjpeg.py: applies the simplified JPEG implementation to an image file.

Unit Tests
----------

You can run the unit tests against the code in this repository by running:

```
    $ python -m unittest discover
```

Packages
--------

The list of packages in this repository and their purposes is below:

* adaptive: contains the adaptive arithmetic coding implementation.
* arithmetic: contains the standard arithmetic coding implementation.
* binary: contains utility methods for converting bits to/from coded bytes when you need to be careful not to add trailing zeros, as this could cause confusion with other codewords. Also contains utilities to convert from strings to/from their literal bit string representation and to convert bit strings to/from their literal byte representations.
* colourspace: contains transform methods for converting RGB pixels to/from the YUV colour space, and a transform method for applying a pixel-by-pixel transformation to each pixel in an image's matrix representation.
* console: contains a method to print a progress bar to the console, useful for the LZ77 implementation which can be quite slow because of the window size used.
* data: contains sample data files used for testing compression algorithms.
* dct: contains an implementation of the discrete cosine transform and its inverse.
* deflate: contains an implementation of the DEFLATE algorithm for a given input sequence.
* encode: encodes and decodes an input string to/from a sequence of bits for a given variable length codebook.
* fano: the Fano method implementation of Shannon-Fano coding.
* huffman: the methods for constructing a Huffman tree and generating a Huffman codebook for a given probability distribution.
* images: contains sample image files used in testing the JPEG implementation.
* jpeg: the simplified JPEG implementation, alongside JPEG-specific utility methods for block-splitting, quantising and loading/saving images.
* lz77: the LZ77 encoder and decoder implementation.
* rle: the run-length encoding implementation used by the JPEG package.
* shannon: the Shannon method implementation of Shannon-Fano coding.
* stats: contains utility methods for statistics operations, in this case for calculating the entropy from a given input probability distribution.
* tree: utilities for displaying trees, converting trees to/from codebooks and converting trees to/from Newick form, among other tree-related utilities.
* util: contains utility methods including cumulate cumulating and input list of elements, calculating the difference of two sets, getting the codeword lengths from an input probability distribution.
* xtree: utilities for working with the extended tree representation.
* zigzag: utilities for ordering a matrix's elements in zigzag order as required by the JPEG encoder.

import unittest
from .arithmetic import arithmetic


class TestArithmeticCodingWorking(unittest.TestCase):

    def testCorrectCodesReturned(self):
        p = [0.2, 0.45, 0.35]
        ciphertext = arithmetic('A', p, ['A', 'B', 'C'])
        self.assertEqual(ciphertext, '0001')


if __name__ == '__main__':
    unittest.main()

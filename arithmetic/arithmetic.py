from math import ceil, floor
from util import cumulate


def arithmetic(input, probabilities, alphabet=None):
    # arith_encode(x,p) implements the binary arithmetic encoder for the source
    # sequence x using the memoryless probability model p.
    #
    # Copyright Jossy 1994, inspired by Witten, Neal & Cleary 1987
    # Translation Joshua 2017

    # define the integer versions of 1.0, 0.5, 0.25 and 0.75

    # 32 bit integer values
    precision = 32

    # this is the max_integer
    one = 2 ** precision - 1
    quarter = ceil(one/4)
    half = 2 * quarter
    three_quarters = 3 * quarter

    # check input alphabet
    if alphabet is not None:
        if not len(alphabet) == len(probabilities):
            raise ValueError('Alphabet size does not match probability distribution')
    else:
        alphabet = range(0, len(probabilities))

    # check the input probability distribution
    if any(p < 0 for p in probabilities) or abs(sum(probabilities) - 1) > 1e-5:
        raise ValueError('Illegal probability distribution')

    # calculate the cumulative probability distribution
    f = cumulate(probabilities)

    # initialise the output string, interval endpoints and straddle counter
    y = ''
    lo = 0
    hi = one
    straddle = 0

    # MAIN ENCODING ROUTINE
    for k in range(0, len(input)):
        # for every input symbol
    
        #################################################################
        #   PLEASE COMPLETE NUMBERED STEPS BELOW AS INSTRUCTED          #
        #################################################################

        # 1) calculate the interval range to be the difference between hi and lo + 1
        # The +1 is necessary to avoid rounding issues
        width = hi - lo + 1

        # The following command finds the index of the next input symbol in
        # the cumulative probability distribution f
        ind = alphabet.index(input[k])

        # 2) narrow the interval end-points [lo,hi) to the new range [f,f+p]
        # within the old interval [lo,hi], being careful to round 'innwards' so
        # the code remains prefix-free (you want to use the functions ceil and
        # floor). This will require two instructions

        lo += ceil(f[ind] * width)
        hi = lo + floor(probabilities[ind] * width)

        # check that the interval has not narrowed to 0
        if hi == lo:
            raise ValueError('Interval has become zero: check that you have no zero probs and increase precision')

        # Now we need to re-scale the interval if its end-points have bits in
        # common, and output the corresponding bits where appropriate

        while True:
            # we will break loop when interval reaches its final state

            if hi < half:
                # lo < hi < 1/2 -> stretch interval and emit 0
                # 3) append a 0 followed by 'straddle' ones to the output
                # string. Reset the straddle counter to zero. The interval
                # re-scaling/stretching will be done after the if-statement.

                y += '0'

                for n in range(0, straddle):
                    y += '1'

                straddle = 0

            elif lo >= half:
                # hi > lo >= 1/2 -> stretch and emit 1
                # 4) append a 1 followed by 'straddle' zeros to the output
                # string and reset the straddle counter

                y += '1'

                for n in range(0, straddle):
                    y += '0'

                straddle = 0

                # 5) take integer 'half' from lo and hi (the actual interval
                # re-stretching will follow after the if statement.
                #
                # in this operation, we want to multiply the endpoints by two
                # and shift them back to the [0, 1) range by subtracting 1
                lo -= half
                hi -= half

            elif (lo >= quarter) and (hi < three_quarters):
                # interval within [1/4,3/4]
                # 6) take integer 'quarter' from lo and hi to prepare for a
                # centered re-stretch, and increase the 'straddle' counter by
                # one.
                lo -= quarter
                hi -= quarter
                straddle += 1

            else:
                # the interval is now stretched to the point where it does not
                # have bits in agreement and we can break the endless loop
                break

            # 7) now multiply the interval end-points by 2 (the -1/2 or -1/4
            # operations have already been performed if appropriate, so the
            # interval is now in all cases within [0,1/2] and can simply be
            # multiplied by 2 to stretch to [0,1]. ADD 1 TO THE HI END-POINT
            # AFTER MULTIPLYING. THIS ENSURES THAT A 1 BIT IS PIPELINED INTO
            # THE HI BOUND AND WILL HELP AVOID UNDERFLOW/OVERFLOW.

            lo *= 2
            hi = hi * 2 + 1

    # Add termination bits to the output string to ensure that the final
    # dyadic interval lies within the source interval
    straddle += 1

    if lo < quarter:
        y += '0'

        for n in range(0, straddle):
            y += '1'
    else:
        y += '1'

        for n in range(0, straddle):
            y += '0'

    return y

import numpy as np
from math import floor

file = open('data/hamlet.txt', 'rb')
file_input = file.read()
file.close()

# the sequence of integers used as bins for the histogram define the edges of the bins, so we need to
# include 256. values are placed into bins according to a <= value < b, where a and b are the edges
# of the relevant bin
hist, bin_edges = np.histogram([data for data in file_input], bins=range(0, 256 + 1))
total = np.sum(hist)
p = [n / total for n in hist]

n = 207039
lo = 0.0
hi = 1.0

for k in range(0, n):
    width = hi - lo
    file_value = int(file_input[k])
    hi = lo + width * sum(p[1:(file_value + 1)])
    lo += width * sum(p[1:file_value])

L = 207039
binary_code = format(floor(lo * 2 ** L), str(L) + 'b')
print(binary_code)

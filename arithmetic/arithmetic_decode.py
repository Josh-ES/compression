from math import ceil, floor
from util import cumulate


def arithmetic_decode(plaintext, probabilities, ny, alphabet=None):
    # arith_decode(x,p) implements the binary arithmetic encoder for the source
    # sequence x using the memoryless probability model p. It attempts to
    # decode ny symbols from the code string x where binary code symbols are
    # drawn from the pointer position xp. The default for xp is 1.
    #
    # Note that the flexibility to decode any number of source symbols means
    # that this can be used to decode the whole source sequence using the model
    # p (e.g. call arith_decode(x,p,file_length,1,1)) or to decode one symbol
    # at a time, say using a different model p for every symbol (as you would
    # in conditional or context-based decoders.)
    #
    # Copyright Jossy 2016, heavily inspired by Witten, Neal & Cleary 1987
    # Translation Joshua 2017

    # define the integer versions of 1.0, 0.5, 0.25 and 0.75

    # 32 bit integer values
    precision = 32

    # this is the max_integer
    one = (2 ** precision) - 1
    quarter = ceil(one / 4)
    half = 2 * quarter
    three_quarters = 3 * quarter

    # check input alphabet
    if alphabet is not None:
        if not len(alphabet) == len(probabilities):
            raise ValueError('Alphabet size does not match probability distribution')
    else:
        alphabet = [chr(i) for i in range(0, len(probabilities))]

    # check the input probability distribution
    if any(p < 0 for p in probabilities) or abs(sum(probabilities) - 1) > 1e-5:
        raise ValueError('Illegal probability distribution')

    # calculate the cumulative probability distribution
    f = cumulate(probabilities)

    # initialise the output string
    y = ''

    # initialise the interval endpoints
    hi = one
    lo = 0

    # add dummy zeros to the input row vector
    for n in range(0, precision):
        plaintext += '0'

    # get the integer value of the string
    value = int(plaintext[0:precision], 2)
    xp = precision
    enumerated_f = list(enumerate(f))

    for k in range(0, ny):
        width = hi - lo + 1
        ind = max([index for index, n in enumerated_f if lo + ceil(n*width) <= value])
        y += alphabet[ind]

        # set the new intervals
        lo += ceil(f[ind] * width)
        hi = lo + floor(probabilities[ind] * width)

        # check that the interval has not narrowed to 0
        if hi == lo:
            raise ValueError('Interval has become zero: check that you have no zero probs and increase precision')

        while True:
            if hi < half:
                # do nothing
                pass
            elif lo >= half:
                lo -= half
                hi -= half
                value -= half
            elif (lo >= quarter) and (hi < three_quarters):
                # interval within [1/4, 3/4)
                lo -= quarter
                hi -= quarter
                value -= quarter
            else:
                break

            lo *= 2
            hi = hi * 2 + 1
            value = value * 2 + int(plaintext[xp], 2)
            xp += 1

            if xp == len(plaintext) + 1:
                raise Exception('Unable to decompress')

    return y

import unittest
from stats import entropy


class TestEntropyMethod(unittest.TestCase):

    def testBernoulliCertain(self):
        self.assertEqual(entropy(1.0), 0)

    def testBernoulliUncertain(self):
        self.assertAlmostEqual(entropy(0.7), 0.88129, places=3)

    def testList(self):
        self.assertAlmostEqual(entropy([0.3, 0.7]), 0.88129, places=3)

if __name__ == '__main__':
    unittest.main()

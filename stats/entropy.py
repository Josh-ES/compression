from math import log
import numbers


def entropy(p):
    # H(p) returns the uncertainty (or entropy) in bits of a random variable
    #  described by the probability vector p.
    #  If p is a matrix, then H(p) returns a row vector where the k-th element
    #  is the entropy of the k-th column of p.
    #
    # Copyright Jossy, 1992
    # Translation Josh, 2017

    if isinstance(p, numbers.Number):
        return h([p, 1 - p])
    else:
        return h(p)


def h(probs):
    # H(p) returns the uncertainty (or entropy) in nats (base e!!!) of a random
    # variable described by the probability vector p.
    #  If p is a matrix, then H(p) returns a row vector where the k-th element
    #  is the entropy of the k-th column of p.

    entropy_probs = probs[:]

    for index, p in enumerate(entropy_probs):
        if p <= 0.0 or p > 1.0:
            entropy_probs[index] = 1.0

    return - sum([p * log(p, 2) for p in entropy_probs])

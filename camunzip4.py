#!/usr/bin/env python3

import adaptive
from sys import argv
from binary import coded_bytes_to_bits


def camunzip4(filename):
    if '.cz4' not in filename:
        raise ValueError('The filename you passed in is not a CamZip 4 file')

    file = open(filename, 'rb')
    ciphertext = file.read()
    file.close()

    file_bytes = []

    for data in ciphertext:
        file_bytes.append(data)

    file_bin = coded_bytes_to_bits(file_bytes)

    # decode the document
    plaintext = adaptive.decode(file_bin)

    # write to the output file in binary
    file = open(filename.replace('.cz4', '.uz4'), 'wb')
    b = bytearray()
    b.extend(map(ord, plaintext))
    file.write(b)
    file.close()


# read the filename from the arguments to the function
f = argv[1]
camunzip4(f)

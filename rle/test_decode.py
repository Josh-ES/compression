import unittest
from .decode import decode


class TestRLEDecoderImplementation(unittest.TestCase):

    def testCorrectStringDecodedFromTuples(self):
        string = decode([('a', 5), ('h', 6), ('m', 7), ('u', 1), ('i', 7), ('a', 6)])
        self.assertEqual(string, 'aaaaahhhhhhmmmmmmmuiiiiiiiaaaaaa')

    def testCorrectStringDecodedFromBinaryString(self):
        string = decode('011000010001000110100000010101101101000110011101010000000110100100011001100001000101')
        self.assertEqual(string, 'aaaaahhhhhhmmmmmmmuiiiiiiiaaaaaa')

    def testCorrectStringDecodedFromBinaryStringWithTrailingZeros(self):
        string = decode('0110000100010001101000000101011011010001100111010100000001101001000110011000010001010000')
        self.assertEqual(string, 'aaaaahhhhhhmmmmmmmuiiiiiiiaaaaaa')

    def testAgainstNumericValues(self):
        binary = '111001100000001111110100000000000000000000111111010000001111111000000011111010000000000000100000001' \
                 '111110000000000000001000000111111010000000000000100000100000101000000000000010000000000001000000011' \
                 '111111000000000000010000001111111100000000000010000000000000000001001111111100000100000000100101'

        numbers = decode(binary, numeric=True)

        self.assertEqual(numbers,
                         [-26, -3, 0, -3, -2, -6, 2, -4, 1, -3, 1, 1, 5, 1, 2, -1, 1, -1, 2, 0, 0, 0, 0, 0, -1, -1, 0,
                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0, 0])


if __name__ == '__main__':
    unittest.main()

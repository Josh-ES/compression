from bitstring import Bits


def decode(input_symbols, length_bits=6, numeric=False):
    # decode(input_symbols) takes an RLE encoded input, either an array of
    # tuples or a binary sequence string, and returns the decoded output
    # string
    #
    # Copyright Joshua 2017

    # if we received a binary string, decode that string and recurse
    if type(input_symbols) == str:
        char_bits = 8
        symbols = []

        for i in range(0, len(input_symbols), char_bits + length_bits):
            if i + char_bits + length_bits <= len(input_symbols):
                # extract binary for each character and repetition number
                char_bin = input_symbols[i:i + char_bits]
                repetition_bin = input_symbols[i + char_bits:i + char_bits + length_bits]

                # extract the character itself and repetition number from the binary codes
                char = chr(int(char_bin, 2)) if not numeric else Bits(bin=char_bin).int
                repetition = int(repetition_bin, 2) + 1
                symbols.append((char, repetition))

        return decode(symbols, length_bits, numeric)

    # otherwise, we have an array of tuples which we can decode easily
    output = '' if not numeric else []

    for char, count in input_symbols:
        if numeric:
            output += [char] * count
        else:
            output += char * count

    return output

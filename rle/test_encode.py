import unittest
from .encode import encode


class TestRLEEncoderImplementation(unittest.TestCase):

    def testCorrectTuplesReturned(self):
        symbols, binary = encode('aaaaahhhhhhmmmmmmmuiiiiiiiaaaaaa')
        self.assertEqual(symbols, [('a', 5), ('h', 6), ('m', 7), ('u', 1), ('i', 7), ('a', 6)])

    def testCorrectBinaryReturned(self):
        symbols, binary = encode('aaaaahhhhhhmmmmmmmuiiiiiiiaaaaaa')
        self.assertEqual(binary, '011000010001000110100000010101101101000110011101010000000110100100011001100001000101')

    def testAgainstAListOfNumbers(self):
        elements = [-26, -3, 0, -3, -2, -6, 2, -4, 1, -3, 1, 1, 5, 1, 2, -1, 1, -1, 2, 0, 0, 0, 0, 0, -1, -1, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0]

        symbols, binary = encode(elements, numeric=True)

        self.assertEqual(symbols,
                         [(-26, 1), (-3, 1), (0, 1), (-3, 1), (-2, 1), (-6, 1), (2, 1), (-4, 1), (1, 1), (-3, 1),
                          (1, 2), (5, 1), (1, 1), (2, 1), (-1, 1), (1, 1), (-1, 1), (2, 1), (0, 5), (-1, 2), (0, 38)])

        self.assertEqual(binary,
                         '11100110000000111111010000000000000000000011111101000000111111100000001111101000000000000010'
                         '00000011111100000000000000010000001111110100000000000001000001000001010000000000000100000000'
                         '00001000000011111111000000000000010000001111111100000000000010000000000000000001001111111100'
                         '000100000000100101')

    def testAgainstAllZeros(self):
        elements = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        symbols, binary = encode(elements, numeric=True)

        self.assertEqual(binary, '00000000111111')


if __name__ == '__main__':
    unittest.main()

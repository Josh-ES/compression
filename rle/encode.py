from bitstring import Bits


def encode(input_values, length_bits=6, numeric=False):
    # encode(input_string) performs run length encoding on the input string
    # passed as an argument, and returns its encoded form in both tuple form
    # and in binary form
    #
    # built for encoding strings of 64 characters, so use 6 bits for the length
    # by default. use 8 bits for the character/symbol itself
    #
    # Copyright Joshua 2017

    count = 1
    prev = None
    symbols = []
    binary = ''

    for char in input_values:
        if char != prev:
            if prev is not None:
                # add the previous symbol, and reset the counts
                symbols.append((prev, count))

                if numeric:
                    binary += Bits(int=prev, length=8).bin
                else:
                    binary += '{0:08b}'.format(ord(prev))

                # we can offset the count by one so it always fits within the number of bits given
                binary += '{0:0{1}b}'.format(count - 1, length_bits)

            count = 1
            prev = char
        else:
            count += 1
    else:
        # add the final character to the list of symbols and to the binary
        symbols.append((prev, count))

        if numeric:
            binary += Bits(int=prev, length=8).bin
        else:
            binary += '{0:08b}'.format(ord(prev))

        # we can offset the count by one so it always fits within the number of bits given
        binary += '{0:0{1}b}'.format(count - 1, length_bits)

    return symbols, binary

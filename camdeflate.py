#!/usr/bin/env python3

import numpy as np
from binary import bits_to_coded_bytes
from deflate import deflate
from stats import entropy
from sys import argv


def camdeflate(filename):
    file = open(filename, 'rb')
    file_input = file.read()
    file.close()

    # we need the probability distribution of the lz77 encoded data
    hist, bin_edges = np.histogram([data for data in file_input], bins=range(0, 256 + 1))
    total = np.sum(hist)
    p_source = [n / total for n in hist]

    # apply the deflate algorithm
    encoded_bits, codes = deflate(file_input)

    # and use bits to bytes to convert back to bytes
    deflated = bits_to_coded_bytes(encoded_bits)

    print('Actual Compression Performance:', len(deflated) / len(file_input) * 8, 'bits per byte')
    print('Theoretical Compression Performance:', entropy(p_source), 'bits per byte')

    # write the binary code to a file
    file = open(filename + '.def', 'wb')
    file.write(bytearray(deflated))
    file.close()

    # write the code table to a file
    file = open(filename + '.defc', 'w')

    for cwd in codes:
        file.write("%s\n" % cwd)

    file.close()


# read the filename from the arguments to the function
f = argv[1]
camdeflate(f)

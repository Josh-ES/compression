#!/usr/bin/env python3

import numpy as np
from binary import bits_to_coded_bytes
from stats import entropy
from sys import argv
from lz77 import lz77_encode


def camlz(filename):
    file = open(filename, 'rb')
    file_input = file.read()
    file.close()

    # we no longer need to generate a probability distribution to encode, but we will just to calculate
    # the entropy
    #
    # the sequence of integers used as bins for the histogram define the edges of the bins, so we need to
    # include 256. values are placed into bins according to a <= value < b, where a and b are the edges
    # of the relevant bin
    hist, bin_edges = np.histogram([data for data in file_input], bins=range(0, 256 + 1))
    total = np.sum(hist)
    p = [n / total for n in hist]

    out = lz77_encode(file_input)
    lz = bits_to_coded_bytes(out)

    print('Actual Compression Performance:', len(lz) / len(file_input) * 8, 'bits per byte')
    print('Theoretical Compression Performance:', entropy(p), 'bits per byte')

    # write the binary code to a file
    file = open(filename + '.clz', 'wb')
    file.write(bytearray(lz))
    file.close()


# read the filename from the arguments to the function
f = argv[1]
camlz(f)

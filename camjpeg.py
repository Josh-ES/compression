#!/usr/bin/env python3

import numpy as np
from binary import bits_to_coded_bytes
from jpeg import jpeg, load_image
from stats import entropy
from sys import argv


def camjpeg(filename):
    file = open(filename, 'rb')
    file_input = file.read()
    file.close()

    # we need the probability distribution of the lz77 encoded data
    hist, bin_edges = np.histogram([data for data in file_input], bins=range(0, 256 + 1))
    total = np.sum(hist)
    p_source = [n / total for n in hist]

    # load the image in RGB colour
    image = load_image(filename)

    # apply the deflate algorithm
    shape, codes, encoded_bits = jpeg(image)

    # and use bits to bytes to convert back to bytes
    jpeg_encoded = bits_to_coded_bytes(encoded_bits)

    print('Actual Compression Performance:', len(jpeg_encoded) / len(file_input) * 8, 'bits per byte')
    print('Theoretical Compression Performance:', entropy(p_source), 'bits per byte')

    # write the binary code to a file
    file = open(filename + '.camjpeg', 'wb')
    file.write(bytearray(jpeg_encoded))
    file.close()

    # write the code table to a file
    file = open(filename + '.camcode', 'w')

    file.write('%d %d\n' % (shape[0], shape[1]))

    for cwd in codes:
        file.write('%s\n' % cwd)

    file.close()


# read the filename from the arguments to the function
f = argv[1]
camjpeg(f)

import numpy as np
from math import cos, pi
from .alpha import alpha


def dct(matrix, value_range=(0, 256)):
    # dct(matrix) performs a discrete cosine transform on the input
    # matrix. The method expects an 8x8 input matrix, with values in
    # the interval [0, 256). The values must first be shifted so they
    # are centred around zero, in other words within the range
    # [-128, 128).

    shift = (value_range[1] - value_range[0]) / 2

    shifted = matrix - shift

    shape = np.shape(shifted)

    output = np.zeros(shape)

    # u is horizontal spacial frequency, v is vertical spacial
    # frequency
    for v in range(0, shape[0]):
        for u in range(0, shape[1]):
            # calculate the value of the output at this point, which
            # requires sums along the x- and y-axes
            g = 0

            # sum according to the formula along the x- and y- axes
            for y in range(0, shape[0]):
                for x in range(0, shape[1]):
                    point_val = shifted[y, x]
                    point_val *= cos(((2 * x + 1) * u * pi) / 16)
                    point_val *= cos(((2 * y + 1) * v * pi) / 16)
                    g += point_val

            # multiply by one quarter and the product of the alpha
            # values for u and v
            g *= 0.25 * alpha(u) * alpha(v)

            # set the value of the output to G
            output[v, u] = g

    return output

import unittest
import numpy as np
from .dct import dct


class TestDiscreteCosineTransformer(unittest.TestCase):

    def testCorrectMatrixReturned(self):
        input_matrix = np.matrix([
            [52, 55, 61, 66, 70, 61, 64, 73],
            [63, 59, 55, 90, 109, 85, 69, 72],
            [62, 59, 68, 113, 144, 104, 66, 73],
            [63, 58, 71, 122, 154, 106, 70, 69],
            [67, 61, 68, 104, 126, 88, 68, 70],
            [79, 65, 60, 70, 77, 68, 58, 75],
            [85, 71, 64, 59, 55, 61, 65, 83],
            [87, 79, 69, 68, 65, 76, 78, 94],
        ])

        transformed_matrix = dct(input_matrix)

        expected_matrix = np.matrix([
            [-415.38, -30.19, -61.20, 27.24, 56.12, -20.10, -2.39, 0.46],
            [4.47, -21.86, -60.76, 10.25, 13.15, -7.09, -8.54, 4.88],
            [-46.83, 7.37, 77.13, -24.56, -28.91, 9.93, 5.42, -5.65],
            [-48.53, 12.07, 34.10, -14.76, -10.24, 6.30, 1.83, 1.95],
            [12.12, -6.55, -13.20, -3.95, -1.87, 1.75, -2.79, 3.14],
            [-7.73, 2.91, 2.38, -5.94, -2.38, 0.94, 4.30, 1.85],
            [-1.03, 0.18, 0.42, -2.42, -0.88, -3.02, 4.12, -0.66],
            [-0.17, 0.14, -1.07, -4.19, -1.17, -0.10, 0.50, 1.68],
        ])

        # make a number of assertions about individual elements in the matrix
        np.testing.assert_array_almost_equal(transformed_matrix, expected_matrix, decimal=2)


if __name__ == '__main__':
    unittest.main()

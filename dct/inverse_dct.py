import numpy as np
from math import cos, pi
from .alpha import alpha


def inverse_dct(matrix, value_range=(0, 256)):
    # dct(matrix) performs an inverse discrete cosine transform on the
    # input matrix. The method expects an 8x8 input matrix, with values in
    # the interval [-128, 128). The values must be shifted at the end so they
    # are centred around the original value range [0, 256).

    shape = np.shape(matrix)

    output = np.zeros(shape)

    # x and y are the positions in the output matrix
    for y in range(0, shape[0]):
        for x in range(0, shape[1]):
            # calculate the value of the output at this point, which
            # requires sums along the u- and v- axes in the DCT matrix
            f = 0

            # sum according the formula along the u- and v- axes
            for v in range(0, shape[0]):
                for u in range(0, shape[1]):
                    point_val = matrix[v, u]
                    point_val *= alpha(u)
                    point_val *= alpha(v)
                    point_val *= cos(((2 * x + 1) * u * pi) / 16)
                    point_val *= cos(((2 * y + 1) * v * pi) / 16)
                    f += point_val

            # multiply by one quarter
            f *= 0.25

            # set the value of the output to g
            output[y, x] = f

    # shift the matrix back into the value range [0, 256)
    shift = (value_range[1] - value_range[0]) / 2
    shifted = output + shift

    return shifted

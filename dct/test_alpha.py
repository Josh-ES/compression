import unittest
from math import sqrt
from .alpha import alpha


class TestDiscreteCosineTransformerAlphaFunction(unittest.TestCase):

    def testCorrectValueForZeroInput(self):
        val = alpha(0)
        self.assertEqual(val, 1 / sqrt(2))

    def testCorrectValueForNonZero(self):
        val = alpha(28)
        self.assertEqual(val, 1)


if __name__ == '__main__':
    unittest.main()

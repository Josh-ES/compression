#!/usr/bin/env python3

from encode import vl_decode
from sys import argv
from binary import coded_bytes_to_bits


def camunzip2(filename):
    if '.cz2' not in filename:
        raise ValueError('The filename you passed in is not a CamZip 2 file')

    file = open(filename, 'rb')
    ciphertext = file.read()
    file.close()
    file_bytes = []

    for data in ciphertext:
        file_bytes.append(data)

    file_bin = coded_bytes_to_bits(file_bytes)

    file = open(filename + 'c', 'r')
    codes = [line.rstrip() for line in file.readlines()]
    file.close()

    # decode the document
    plaintext = vl_decode(file_bin, codes)

    # write to the output file in binary
    file = open(filename.replace('.cz2', '.output'), 'wb')
    b = bytearray()
    b.extend(map(ord, plaintext))
    file.write(b)
    file.close()

# read the filename from the arguments to the function
filename = argv[1]
camunzip2(filename)
